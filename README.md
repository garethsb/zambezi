![ZLOGO] Zambezi
--------------

A low-level SMB2/3 message handling library.

This project encapsulates the marshalling and unmarshalling of SMB2/3
messages.

The goal is to create a basic library, with a sensible API, that can
provide the foundation layer of an SMB2/3 client or server implementation.

Licenses
--------

The source code is provided under two licenses.
1.  Except for unit tests, code that compiles into a runable program will
    typically be licensed under the GNU Affero General Public License
    (AGPLv3).
2.  Code that is intended to be part of the Zambezi library, associated unit
    tests, and the Doxygen documentation for that code, will typically be
    licensed under the GNU Lesser General Public License (LGPLv3).

The individual files will contain specific license statements as shown below.

<b>Programs</b>

    This program is part of Zambezi.

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
    License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

<b>Library and Associated Documentation</b>

    This file is part of Zambezi.

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 3.0 of the License, or (at
    your option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this library.  If not, see <http://www.gnu.org/licenses/>.

Licensing Philosophy
--------------------

The plan is to submit the API developed by this project to a standardization
process, which will likely result in a modified (hopefully, improved) API.
This code would then be updated to match the new API, and would become a
reference implementation.  The reference implementation, in turn, would be
released under an additional license that would make it suitable for
widespread adoption across a wide spectrum of implementations.

In other words, if all of the pieces fall into place, this code will be
released under additional licenses.  <b><em>Don't send in any patches
unless you're okay with that.</em></b>

Docs
----

Zambezi is [Doxygenated](https://ubiqx.gitlab.io/zambezi/).\
There is also a [wiki](https://gitlab.com/ubiqx/zambezi/-/wikis/home) under
construction.

We consider the project documentation to be as important as the code
itself.  The goal is to provide clear and usuable API information, plus SMB3
protocol insights and tips.  The Doxygen docs should provide context and
references, and the wiki should provide a sense of what's going on.

Words
-----

<dl>
  <dt>CIFS</dt>
  <dd>
  <b>C</b>ommon <b>I</b>nternet <b>F</b>ile <b>S</b>ystem.

  CIFS is the "marketing upgrade" name that was bestowed upon the SMB
  protocol in the mid-1990's. For a while, there was speculation that a new
  dialect of SMB would be released with Windows 2000, but that never
  actually happened.  Instead of being forward-looking, CIFS became an
  intermim name for what we now call SMB1 (see below).  The term CIFS is now
  considered dead.  Microsoft doesn't use it any more, except in older
  documentation or references to legacy implementations. The term is still
  popular among sales engineers.  (See [[IMPCIFS]] and [[MS-CIFS]].)
  </dd>

  <dt>NBT</dt>
  <dd>
  <b>N</b>et<b>B</b>IOS over <b>T</b>CP transport protocol.

  NBT is the virtual LAN protocol specified by IETF Standard #19, which
  consists of RFC1001 and RFC1002.  NBT provides a mechanism for supporting
  the semantics of the NetBIOS API over TCP/UDP/IP internetworks.  (See
  [[IMPCIFS]], [[NBGUIDE]], [[RFC1001]], [[RFC1002]], and [[MS-NBTE]].)
  </dd>

  <dt>SMB</dt>
  <dd>
  <b>S</b>erver <b>M</b>essage <b>B</b>lock protocol.

  SMB was originally created by IBM in the early 1980's for use with
  PC-DOS.  It was later updated and extended for use with OS/2, and marketed
  under the name LAN Manager.  OS/2 LAN Manager was eventually ported to
  Windows NT, where it was called NT LAN Manager (NTLM).  The ported dialect
  was identified as "NT LM 0.12".  (See [[IMPCIFS]], [[MS-CIFS]], and
  [[MS-SMB]].)
  </dd>

  <dt>SMB1</dt>
  <dd>
  Server Message Block protocol, Version <b>1</b>.

  Also known as SMBv1, CIFS, or "NT LM 0.12"; SMB1 is the designation given
  to the "NT LM 0.12" dialect of the original SMB protocol as implemented in
  all versions of Windows since Windows NT 3.51.  The SMB1 name may be
  thought of as excluding older OS/2 LAN Manger and DOS versions of SMB,
  except that the Windows NT LAN Manager implementation is (mostly) backward
  compatible with those older versions.  (See [[MS-CIFS]]; Changes to SMB1
  between Windows NT4 and all subsequent Windows versions are documented in
  [[MS-SMB]].)
  </dd>

  <dt>SMB2</dt>
  <dd>
  Server Message Block protocol, Version <b>2</b>.

  SMB2 is not a dialect of SMB1, it is a different protocol.  It does,
  however, share many characteristics with SMB1. SMB2 was introduced in
  Windows Vista.  This original release was not particularly ambitious, and
  its existence was hardly noticed at the time. SMBv2.1 was released with
  Windows 7.  SMB2.2 was scheduled for release with Windows 8, but see the
  description of SMB3.  (See [[MS-SMB2]].)
  </dd>

  <dt>SMB3</dt>
  <dd>
  Server Message Block protocol, Version <b>3</b>.

  SMB3 is just a set of newer dialects of SMB2.  SMB3 was originally
  intended to be released as SMB version 2.2 but it contained a number of
  significant new features and so was re-dubbed SMB3 (another "marketing
  upgrade").  SMB3 supports I/O over RDMA as well as scale-out and failover
  clustering.  (SMB3 is documented in [[MS-SMB2]].)
  </dd>
</dl>

<!-- Link Identifiers (shorthand).  These do not work in dt blocks. :-( -->
[IMPCIFS]:  http://www.ubiqx.org/cifs/
[MS-CIFS]:  https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-cifs/d416ff7c-c536-406e-a951-4f04b2fd1d2b
[MS-DFSC]:  https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-dfsc/3109f4be-2dbb-42c9-9b8e-0b34f7a2135e
[MS-NBTE]:  https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-nbte/3461cfa8-3d28-4fa3-8163-131bf1046fa3
[MS-SMB]:   https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-smb/f210069c-7086-4dc2-885e-861d837df688
[MS-SMB2]:  https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/5606ad47-5ee0-437a-817e-70c366052962
[NBGUIDE]:  https://web.archive.org/web/20170724042731/http://www.netbiosguide.com:80/
[RFC1001]:  http://www.ietf.org/rfc/rfc1001.txt
[RFC1002]:  http://www.ietf.org/rfc/rfc1002.txt
[SNIASMB3]: https://www.snia.org/smb3
[STD19]:    https://www.rfc-editor.org/info/std19
[ZLOGO]:    vanity/Zambezi.ProjectLogo.png

References
----------

All of the references listed below are available online.

<dl>
  <dt>[[IMPCIFS]](http://www.ubiqx.org/cifs/)</dt>
    <dd>
    Hertel, Christopher R., "Implementing CIFS - The Common Internet File
    System", Prentice Hall, August 2003, ISBN:013047116X
    </dd>
  <dt>[[MS-CIFS]](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-cifs/)</dt>
    <dd>
    Microsoft Corporation, "Common Internet File System (CIFS) Protocol
    Specification"
    </dd>
  <dt>[[MS-DFSC]](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-dfsc/)</dt>
    <dd>
    Microsoft Corporation, "Distributed File System (DFS): Referral Protocol
    Specification"
    </dd>
  </dd>
  <dt>[[MS-NBTE]](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-nbte/)</dt>
  <dd>
    Microsoft Corporation, "NetBIOS over TCP (NBT) Extensions"
  </dd>
  <dt>[[MS-SMB]](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-smb/)</dt>
    <dd>
    Microsoft Corporation, "Server Message Block (SMB) Protocol Specification"
    </dd>
  <dt>[[MS-SMB2]](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/)</dt>
    <dd>
    Microsoft Corporation, "Server Message Block (SMB) Protocol Versions 2
    and 3"
    </dd>
  <dt>[[MS-WINERRATA]](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-winerrata/)</dt>
    <dd>
    Microsoft Corporation, "Windows Protocols Errata"
  </dd>
  <dt>[[NBGUIDE]](https://web.archive.org/web/20170724042731/http://www.netbiosguide.com:80/)</dt>
    <dd>
    Winston, Gavin, "NetBIOS Specification", 1998-2012 (Archived)
    </dd>
  <dt>[[RFC1001]](http://www.ietf.org/rfc/rfc1001.txt)</dt>
    <dd>
    Network Working Group, "Protocol Standard for a NetBIOS Service on a
    TCP/UDP Transport: Concepts and Methods", STD 19, RFC 1001, March 1987
    </dd>
  <dt>[[RFC1002]](http://www.ietf.org/rfc/rfc1002.txt)</dt>
    <dd>
    Network Working Group, "Protocol Standard for a NetBIOS Service on a
    TCP/UDP Transport: Detailed Specifications", STD 19, RFC 1002, March 1987
    </dd>
  <dt>[[STD19]](https://www.rfc-editor.org/info/std19)</dt>
    <dd>
    [RFC1001] and [RFC1002] together comprise IETF STD 19.
    </dd>
  <dt>[[SMBURL]](https://tools.ietf.org/html/draft-crhertel-smb-url-12)</dt>
    <dd>
    Draft SMB URL specification (expired).
    </dd>
  <dt>[[SNIASMB3]](https://www.snia.org/smb3)</dt>
    <dd>
    The Storage Networking Industry Association (SNIA) has put together an
    SMB3 Reference website.
    </dd>
</dl>

Naming
------

Zambezi is the name of a river in Africa, and possibly the name of a
forgotten twelfth century antarctic poet<sup>1</sup>.  Neither of these
facts are relevant.  The project just needed a name.  (Ep39s7.)

*"What's in a name?  A rose by any other name would wither and die."*<br>
-- Alan Swann (Peter O'Toole),
[My Favorite Year](https://en.wikipedia.org/wiki/My_Favorite_Year), 1982

__
<br><sup>1</sup>Probably not.

Epitath
-------
You have been eaten by a grue.

Timestamp
---------
$Id: README.md; 2021-03-03 13:24:37 -0600; crh$
