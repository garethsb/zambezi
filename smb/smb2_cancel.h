#ifndef SMB2_CANCEL_H
#define SMB2_CANCEL_H
/* ========================================================================== **
 *                               smb2_cancel.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Marshall/unmarshall SMB2/3 Cancel requests.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb2_cancel.h; 2020-09-22 22:14:05 -0500; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file    smb2_cancel.h
 * @author  Christopher R. Hertel
 * @brief   Parse/pack cancel request messages.
 * @date    18 Sep 2020
 * @version \$Id: smb2_cancel.h; 2020-09-22 22:14:05 -0500; crh$
 * @copyright Copyright (C) 2020 by Christopher R. Hertel
 *
 * @details
 *  There is \b no SMB2 Cancel Response message, only a Request.
 *
 *  There is never a response to the Cancel Request itself.  The response
 *  is sent in reference to the command indicated in the \c #smb2_Header
 *  of the Cancel Request.  The documentation refers to this as the
 *  "target request".
 *  - If the target request cannot be found on the server, the server
 *    should quietly ignore the Cancel Request.
 *  - If the target request \e is found...
 *    - If the Cancel is successful, the response to the target request
 *      will be an Error Response message with a status of
 *      \c #STATUS_CANCELLED.
 *    - If the operation could not be cancelled, then it will continue to
 *      run and send a response in the normal way as if no Cancel Request
 *      had been sent.
 *
 *  The response, if any, is sent in reference to the original target
 *  request.
 *
 * @see <a href="@msdocs/ms-smb2/57bae3d3-5dd7-4a5f-92cb-fc52e2087dad">[MS-SMB2;
 *      3.3.5.16]: Receiving an SMB2 CANCEL Request</a>
 */

#include "smb2_baseMsg.h"


/* -------------------------------------------------------------------------- **
 * Typedefs
 */

/**
 * @typedef smb2_CancelReq
 * @brief   SMB2/3 Cancel Request message structure.
 * @details This maps directly to a \c #smb2_BaseMsg.
 * @see     #smb2_BaseMsg
 */
typedef smb2_BaseMsg smb2_CancelReq;


/* -------------------------------------------------------------------------- **
 * Function Prototypes
 */

int smb2_parseCancelReq( uint16_t        const dialect,
                         uint8_t        *const msg,
                         size_t          const msgLen,
                         smb2_CancelReq *const cancelReq );

int smb2_packCancelReq( uint16_t        const dialect,
                        smb2_CancelReq *const cancelReq,
                        uint8_t        *const bufr,
                        uint32_t        const bSize );


/* ============================= smb2_Cancel.h ============================== */
#endif /* SMB2_Cancel_H */
