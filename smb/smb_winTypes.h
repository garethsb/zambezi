#ifndef SMB_WINTYPES_H
#define SMB_WINTYPES_H
/* ========================================================================== **
 *                               smb_winTypes.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Support for various Windows-compatible data types.
 *
 * Copyright (C) 2019 by Christopher R. Hertel
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file      smb_winTypes.h
 * @author    Christopher R. Hertel
 * @brief     Implementations of several Windows-compatible SMB datatypes.
 * @date      12 Apr 2019
 * @version   \$Id: smb_winTypes.h; 2020-08-15 23:18:38 -0500; crh$
 * @copyright Copyright (C) 2019 by Christopher R. Hertel
 *
 * @details
 *  This module is a grab-bag of data types used in SMB2/3 but not native
 *  to other systems, particularly POSIX.
 *
 *  <b>Time Format Conversions</b>
 *
 *  Windows and POSIX store time values very differently.
 *
 *  The Windows epoch is 01-01-1601-00:00:00; Midnight, January 1st, 1601.
 *  Windows measures time in units of 10^-7 seconds.  There is no official
 *  prefix for these units, but "bozo-" has been proposed.  Thus,
 *  "bozoseconds".  One bozosecond is equal to 100 nanoseconds, or one
 *  tenth of a microsecond.
 *
 *  The POSIX epoch is 01-01-1970 00:00:00, 369 years latar than Windows.
 *  The POSIX <tt>struct timespec</tt> format splits the timestamp into
 *  seconds and nanoseconds.  Seconds are stored in a \c time_t integer,
 *  and nanoseconds are stored in a \c long.  Both fields are signed values,
 *  allowing for dates prior to the POSIX epoch.
 *
 *  SMB reports time in the Windows \c FILETIME format, which is defined
 *  as a pair of DWORD values.  A DWORD, in turn is defined as an unsigned
 *  32-bit integer.  In practical terms, however, a \c FILETIME value is an
 *  unsigned 64-bit integer.
 *
 *  This set of functions is used to convert between \c FILETIME and POSIX
 *  <tt>struct timespec</tt> format.  The <tt>struct timespec</tt> structure
 *  is described in <tt>time.h</tt> and sometimes in <tt>clock_gettime(2)</tt>.
 *
 * @see smb_endian.h
 * @see <a href="@posix/basedefs/time.h.html">time.h</a>
 * @see <a href="@posix/functions/clock_gettime.html">clock_gettime(2)</a>
 * @see <a href="@msdocs/ms-dtyp/262627d8-3418-4627-9218-4ffe110850b2">[MS-DTYP;
 *  2.2.9]: DWORD</a>
 * @see <a href="@msdocs/ms-dtyp/2c57429b-fdd4-488f-b5fc-9e4cf020fcdf">[MS-DTYP;
 *  2.3.3]: FILETYPE</a>
 * @see Wikipedia: <a href="https://en.wikipedia.org/wiki/NTFS">NTFS</a>
 * @see Wikipedia: <a href="https://en.wikipedia.org/wiki/1601">1601</a>
 * @see <a href="http://ubiqx.org/cifs/SMB.html#SMB.6.3.1">Implementing CIFS,
 *  section 2.6.3.1</a>, under "SystemTimeLow and SystemTimeHigh"
 *
 * @todo
 *  - Verify that we properly handle 64 vs. 32 bit values.
 */


/* -------------------------------------------------------------------------- **
 * Exported Functions
 */

struct timespec ftime2tspec( uint64_t const ftime );

uint64_t tspec2ftime( struct timespec pt );

uint64_t ftimeofday( void );

/* ============================= smb_winTypes.h ============================= */
#endif /* SMB_WINTYPES_H */
