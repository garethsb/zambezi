#ifndef SMB2_TREECONN_H
#define SMB2_TREECONN_H
/* ========================================================================== **
 *                               smb2_treeconn.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: SMB2/SMB3 Tree Connect and Disconnect
 *
 * Copyright (C) 2021 by Christopher R. Hertel
 * $Id: smb2_treeconn.h; 2021-03-09 17:17:06 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file    smb2_treeconn.h
 * @author  Christopher R. Hertel
 * @brief   SMB2/3 Tree Connect and Disconnect
 * @date    4 Nov 2020
 * @version \$Id: smb2_treeconn.h; 2021-03-09 17:17:06 -0600; crh$
 * @copyright Copyright (C) 2020 by Christopher R. Hertel
 *
 * @details
 *  The Tree Connect is the moral equivalent of a `mount` operation on
 *  POSIXy systems like BSD and Linux.  It makes a connection between a
 *  client system and an SMB Share.  A share, in turn, generally refers
 *  to a directory on the server side, though other filesystem objects
 *  can also be shared (e.g., the $IPC share, which provides access to
 *  named pipes).
 *
 *  The Tree Disconnect request/response messages are simple base messages
 *  (see \c smb2_baseMsg.h).  The Tree Connect messages are just a bit more
 *  complex, particularly in SMBv3.1.1 where context extensions may be
 *  involved.
 *
 * \b Notes
 *  - SMB2 Tree Connect Messages are not particularly complex, until you
 *    dig into them a bit.  The various flags fields hide several secrets.
 *    The flags touch on filesystem encryption, caching models, distributed
 *    file system (DFS), clustering, and a few other advanced features.
 *  - In SMBv3.1.1, Tree Connect contexts have been added to further confuse
 *    and disorient third party developers.
 *
 * @todo
 *  - \b Project-wide \n For all \c pack routines, consider:
 *    \code if( !StructureSize ) StructureSize = <default>; \endcode
 * @todo
 *  - Add support for known Tree Connect Response contexts; <a
 *    href="@msdocs/ms-smb2/06eaaabc-caca-4776-9daf-82439e90dacd">[MS-SMB2;
 *    2.2.9.2]: SMB2 TREE_CONNECT_CONTEXT Request Values</a>
 *  - Research Namespace Caching.
 *    It appears to be related to directory enumeration caching, but the
 *    documentation says that the flag (on the share) must be ignored by
 *    the client.  Is it handled instead on a per-directory (per-directory
 *    handle) basis (since directories need to be opened before they can
 *    be enumerated)?
 *  - The per-share Capabilities bits, which are returned to the caller in
 *    the SMB2 TREE_CONNECT Response message, refer to advanced features
 *    related to distributed (DFS) or clustered shares.  The descriptions
 *    in [MS-SMB2; 2.2.10] are vague and confusing.  More research is
 *    needed in order to clean up the mess and provide useful documentation.
 *
 * @see <a href="@msdocs/ms-smb2/832d2130-22e8-4afb-aafd-b30bb0901798">[MS-SMB2;
 *  2.2.9]: SMB2 TREE_CONNECT Request</a>
 * @see <a href="@msdocs/ms-smb2/dd34e26c-a75e-47fa-aab2-6efc27502e96">[MS-SMB2;
 *  2.2.10]: SMB2 TREE_CONNECT Response</a>
 * @see <a href="@msdocs/ms-swn/1c404bcb-4a19-4152-a465-ec9a27cb717d">[MS-SWN]:
 *  Service Witness Protocol</a>
 * @see <a href="https://www.snia.org/sites/default/files/files2/files2/SDC2013/
 * presentations/SMB3/DavidKruse_SMB3_Update.pdf">SMB3 Update (SDC 2013)</a>
 *  &mdash; A presentation by David Kruse, Microsoft, at the 2013 SNIA
 *  Storge Developer Conference
 */

#include <stdint.h>         /* Standard integer types.  */

#include "smb2_baseMsg.h"   /* Tree Disconnect Req/Resp are Base Messages. */


/* -------------------------------------------------------------------------- **
 * Defined Constants
 *//**
 * @def     SMB2_TREECONN_REQ_SIZE
 * @brief   The size of the fixed portion of the SMB2 Tree Connect Request.
 * @details The length, in bytes, of the fixed-length portion of the SMB2
 *          Tree Connect Request message.
 *
 * @def     SMB2_TREECONN_REQ_STRUCT_SIZE
 * @brief   The structure size of the SMB2 Tree Connect Request message.
 * @details The required \c StructureSize value, which is one more than
 *          the \c #SMB2_TREECONN_REQ_SIZE to indicate the presence of
 *          the \c Buffer (variable portion of the message).
 *
 * @def     SMB2_TREECONN_RESP_SIZE
 * @brief   The size of the fixed portion of the SMB2 Tree Connect Response.
 * @details The length, in bytes, of the fixed-length portion of the SMB2
 *          Tree Connect Response message.
 *
 * @def     SMB2_TREECONN_RESP_STRUCT_SIZE
 * @brief   The structure size of the SMB2 Tree Connect Response message.
 * @details This is the required \c StructureSize value.  The SMB2 Tree
 *          Connect Response has no variable-length portion, so this value
 *          is an even number equal to \c #SMB2_TREECONN_RESP_SIZE.
 *
 * @def     SMB2_TREEDISC_REQ_SIZE
 * @brief   The size of the fixed portion of an SMB2 Tree Disconnect Request.
 * @details The length, in bytes, of the fixed-length portion of an SMB2
 *          Tree Disconnect Request message.
 *
 * @def     SMB2_TREEDISC_REQ_STRUCT_SIZE
 * @brief   The structure size of the SMB2 Tree Disconnect Request message.
 * @details This is the \c StructureSize value required by the protocol.
 *          The \c #smb2_TreeDiscReq.StructureSize field MUST be set to this
 *          value.
 *
 * @def     SMB2_TREEDISC_RESP_SIZE
 * @brief   The fixed portion size of an SMB2 Tree Disconnect Response.
 * @details The length, in bytes, of the fixed-length portion of an SMB2
 *          Tree Disconnect Response message.
 *
 * @def     SMB2_TREEDISC_RESP_STRUCT_SIZE
 * @brief   SMB2 Tree Disconnect \c StructureSize.
 * @details This is the \c StructureSize value required by the protocol.
 *          The \c #smb2_TreeDiscResp.StructureSize field MUST be set to
 *          this value.
 */

#define SMB2_TREECONN_REQ_SIZE          8
#define SMB2_TREECONN_REQ_STRUCT_SIZE   9

#define SMB2_TREECONN_RESP_SIZE         16
#define SMB2_TREECONN_RESP_STRUCT_SIZE  16

#define SMB2_TREEDISC_REQ_SIZE          SMB2_BASEMSG_SIZE
#define SMB2_TREEDISC_REQ_STRUCT_SIZE   SMB2_BASEMSG_STRUCT_SIZE

#define SMB2_TREEDISC_RESP_SIZE         SMB2_BASEMSG_SIZE
#define SMB2_TREEDISC_RESP_STRUCT_SIZE  SMB2_BASEMSG_STRUCT_SIZE


/* -------------------------------------------------------------------------- **
 * Enumerated Constants
 */

/**
 * @enum  SMB2_tcFlags
 *        SMB2 Tree Connect Request flags.
 *
 *  These flags were introduced in the SMBv311 dialect to provide support
 *  for new and updated features, as described per flag.
 *  \n \b Note: These flags are only valid for the \b SMBv311 dialect and
 *              above.  In earlier dialects, the field is Reserved-MB0.
 *
 * @see <a href="@msdocs/ms-smb2/832d2130-22e8-4afb-aafd-b30bb0901798">[MS-SMB2;
 *    2.2.9]: SMB2 TREE_CONNECT Request</a>
 *
 * @var SMB2_TREE_CONNECT_FLAG_CLUSTER_RECONNECT
 *      This flag is set to indicate that the client is reconnecting to
 *      a cluster share to which it had previously connected.
 *
 * @var SMB2_TREE_CONNECT_FLAG_REDIRECT_TO_OWNER
 *      This flag is set to indicate that the client can handle a synchronous
 *      share redirect via a Share Redirect error response.
 *
 * @var SMB2_TREE_CONNECT_FLAG_EXTENSION_PRESENT
 *      This flag is set to indicate that a tree connect request extension
 *      blob is present.  The extension blob will be located at the start
 *      of the \c Buffer field.
 */
typedef enum
  {
  SMB2_TREE_CONNECT_FLAG_CLUSTER_RECONNECT = 0x0001,
  SMB2_TREE_CONNECT_FLAG_REDIRECT_TO_OWNER = 0x0002,
  SMB2_TREE_CONNECT_FLAG_EXTENSION_PRESENT = 0x0004
  } SMB2_tcFlags;

/**
 * @enum  SMB2_tcShareType
 *        Share object type, returned in the Tree Connect Response.
 *
 * @see <a href="@msdocs/ms-smb2/dd34e26c-a75e-47fa-aab2-6efc27502e96">[MS-SMB2;
 *    2.2.10]: SMB2 TREE_CONNECT Response</a>
 */
typedef enum
  {
  SMB2_SHARE_TYPE_DISK  = 0x01, /**< Object Store (disk) share. */
  SMB2_SHARE_TYPE_PIPE  = 0x02, /**< Named pipe share.          */
  SMB2_SHARE_TYPE_PRINT = 0x03, /**< Printer share.             */
  SMB2_SHARE_TYPE_MASK  = 0x03  /**< Bitmask of defined bits.   */
  } SMB2_tcShareType;

/**
 * @enum  SMB2_tcShareFlags
 * @brief Important little things to know about the share.
 * @details
 *  These flags are returned by the server in the Tree Connect Response.
 *
 * @see <a href="@msdocs/ms-smb2/dd34e26c-a75e-47fa-aab2-6efc27502e96">[MS-SMB2;
 *    2.2.10]: SMB2 TREE_CONNECT Response</a>
 *
 * \b Notes
 *  - One of the following caching modes \e must be selected:
 *    + \c #SMB2_SHAREFLAG_NO_CACHING
 *    + \c #SMB2_SHAREFLAG_MANUAL_CACHING
 *    + \c #SMB2_SHAREFLAG_AUTO_CACHING
 *    + \c #SMB2_SHAREFLAG_VDO_CACHING
 *    .
 *    Zero or more of the remaining flags may be enabled.\n
 *    \b Note: The above list of required flags covers two specific bits,
 *    yielding four possible values [0..3].  Mathematically, one of the
 *    above modes \b must, indeed, be selected.
 *
 *  - Both the \c SMB2_SHAREFLAG_DFS and \c SMB2_SHAREFLAGS_DFS_ROOT flags
 *    \e should be set if the share is part of a DFS namespace.  However,
 *    the relevant product behavior notes (in [MS-SMB2; 6]) state that
 *    Windows does not set these bits.
 *    \n **See also**
 *    + <a href="@msdocs/ms-smb2/652e0c14-5014-4470-999d-b174d7b2da87">[MS-SMB2;
 *          3.3.5.7]: Receiving an SMB2 TREE_CONNECT Request</a>
 *    + <a href="@msdocs/ms-smb2/a64e55aa-1152-48e4-8206-edd96444e7f7">[MS-SMB2;
 *          6]: Appendix A: Product Behavior</a>
 *
 * @var SMB2_SHAREFLAG_MANUAL_CACHING
 *      The client is permitted to cache files that are explicitly selected
 *      (by the end user) for "offline" use.
 * @var SMB2_SHAREFLAG_AUTO_CACHING
 *      The client is permitted to automatically cache files if they are
 *      used (by the end user) for offline access.
 * @var SMB2_SHAREFLAG_VDO_CACHING
 *      The client is permitted to automatically cache files that are used
 *      (by the end user) for offline access.  The client is also permitted
 *      to use those files in an offline mode even if the share \e is
 *      currently accessible.
 * @var SMB2_SHAREFLAG_NO_CACHING
 *      The client **must not** cache files for offline use.
 *
 * @var SMB2_SHAREFLAG_DFS
 *      If the share is part of a DFS namespace, the server \e should set
 *      this flag.  See also \c #SMB2_SHAREFLAG_DFS_ROOT. \n
 *      \b Note:  According to [MS-SMB2; 6], Windows sets neither this nor
 *                the \c #SMB2_SHAREFLAG_DFS_ROOT flag.
 * @var SMB2_SHAREFLAG_DFS_ROOT
 *      If the share is part of a DFS namespace, the server \e should set
 *      this flag.  See also \c #SMB2_SHAREFLAG_DFS.\n
 *      \b Note:  According to [MS-SMB2; 6], Windows sets neither this nor
 *                the \c #SMB2_SHAREFLAG_DFS flag.
 *
 * @var SMB2_SHAREFLAG_RESTRICT_EXCLUSIVE_OPENS
 *      The share prevents exclusive file opens that would deny read
 *      operations on an open file.  The server will deny an exclusive open
 *      request that does not include \c FILE_SHARE_READ.
 *      This flag is only valid on \c #SMB2_SHARE_TYPE_DISK shares.
 * @var SMB2_SHAREFLAG_FORCE_SHARED_DELETE
 *      The share prevents exclusive file opens that would prevent deletion
 *      of the file as long as the exclusive open is active.  The server
 *      will deny an exclusive open request that does not include
 *      \c FILE_SHARE_DELETE.
 *      This flag is only valid on \c #SMB2_SHARE_TYPE_DISK shares.
 *
 * @var SMB2_SHAREFLAG_ALLOW_NAMESPACE_CACHING
 *      The client MUST ignore this flag.
 *      @todo It appears that there is a backstory to this flag, and it
 *            probably should be documented, at least minimally.  What,
 *            in this context, is Namespace Caching?
 *
 * @var SMB2_SHAREFLAG_ACCESS_BASED_DIRECTORY_ENUM
 *      When the client is enumerating the contents of a directory, the
 *      server will filter out entries based upon the access permissions
 *      of the client.
 *
 * @var SMB2_SHAREFLAG_FORCE_LEVELII_OPLOCK
 *      The share will not provide exclusive OpLocks for opens.  Requests
 *      for exclusive caching rights will, at best, be degraded to read
 *      caching only.
 *
 * @var SMB2_SHAREFLAG_ENABLE_HASH_V1
 *      BranchCache V1 hashing is available on this share.  V1 hashing is
 *      available in SMB 2.1.0 and above.
 *      \b See <a
 *  href="@msdocs/ms-ccrod/5c1fdcf1-6b60-452a-904c-f650d2edf449">[MS-CCROD]:
 *  Content Caching and Retrieval Protocols Overview</a>
 *
 * @var SMB2_SHAREFLAG_ENABLE_HASH_V2
 *      BranchCache V2 hashing is available on this share. V2 hashing is
 *      available in SMB 3.0.0 and above.
 *      \b See <a
 *  href="@msdocs/ms-ccrod/5c1fdcf1-6b60-452a-904c-f650d2edf449">[MS-CCROD]:
 *  Content Caching and Retrieval Protocols Overview</a>
 *
 * @var SMB2_SHAREFLAG_ENCRYPT_DATA
 *      If set, the server will reject any unencrypted message.  This flag
 *      is valid for SMB 3.0.0 and above.
 *
 * @var SMB2_SHAREFLAG_IDENTITY_REMOTING
 *      The share provides support for the remote identity feature.  The
 *      client may request remoted identity access to the share using
 *      the \c #SMB2_REMOTED_IDENTITY_TREE_CONNECT context.
 *      @todo Research and explain the Remoted Identity feature.
 *
 * @var SMB2_SHAREFLAG_MASK
 *      Bitmask of all currently known ShareFlag bits.
 */
typedef enum
  {
  /* One of the following is required.  */
  SMB2_SHAREFLAG_MANUAL_CACHING               = 0x00000000,
  SMB2_SHAREFLAG_AUTO_CACHING                 = 0x00000010,
  SMB2_SHAREFLAG_VDO_CACHING                  = 0x00000020,
  SMB2_SHAREFLAG_NO_CACHING                   = 0x00000030,
  /* Additional flags.  */
  SMB2_SHAREFLAG_DFS                          = 0x00000001,
  SMB2_SHAREFLAG_DFS_ROOT                     = 0x00000002,
  SMB2_SHAREFLAG_RESTRICT_EXCLUSIVE_OPENS     = 0x00000100,
  SMB2_SHAREFLAG_FORCE_SHARED_DELETE          = 0x00000200,
  SMB2_SHAREFLAG_ALLOW_NAMESPACE_CACHING      = 0x00000400,
  SMB2_SHAREFLAG_ACCESS_BASED_DIRECTORY_ENUM  = 0x00000800,
  SMB2_SHAREFLAG_FORCE_LEVELII_OPLOCK         = 0x00001000,
  SMB2_SHAREFLAG_ENABLE_HASH_V1               = 0x00002000,
  SMB2_SHAREFLAG_ENABLE_HASH_V2               = 0x00004000,
  SMB2_SHAREFLAG_ENCRYPT_DATA                 = 0x00008000,
  SMB2_SHAREFLAG_IDENTITY_REMOTING            = 0x00040000,
  SMB2_SHAREFLAG_MASK                         = 0x0004FF33
  } SMB2_tcShareFlags;

/**
 * @enum  SMB2_tcShareCaps
 * @brief SMB2/3 Share Capability flags.
 * @details
 *  These Capability flags are returned by the server in the Tree Connect
 *  Response.
 *
 * @see <a href="@msdocs/ms-smb2/dd34e26c-a75e-47fa-aab2-6efc27502e96">[MS-SMB2;
 *    2.2.10]: SMB2 TREE_CONNECT Response</a>
 * @see <a href="@msdocs/ms-swn/1c404bcb-4a19-4152-a465-ec9a27cb717d">[MS-SWN]:
 *    Service Witness Protocol</a>
 *
 * @var SMB2_SHARE_CAP_DFS
 *      If the server supports DFS (Distributed File System) and the share
 *      is part of a DFS tree, this bit must be set (1).  Otherwise, it
 *      should be clear.
 *
 * @var SMB2_SHARE_CAP_CONTINUOUS_AVAILABILITY
 *      For SMBv3.0.0 and above, this bit is set (1) if the share has the
 *      Continuous Availability feature enabled.  This bit must clear (0)
 *      in SMBv2.x dialects.
 *
 * @var SMB2_SHARE_CAP_SCALEOUT
 *      This bit mut be clear (0) in SMBv2.x dialects.  For SMBv3.0.0
 *      and above, if the server is part of a scale-out cluster that
 *      supports recoverable handles, this bit should be set (1).
 *
 * @var SMB2_SHARE_CAP_CLUSTER
 *      This bit mut be clear (0) in SMBv2.x dialects.  For SMBv3.0.0
 *      and above, if the Witness protocol is enabled to monitor and
 *      report on the availability of the share, this bit should be
 *      set (1).
 *
 * @var SMB2_SHARE_CAP_ASYMMETRIC
 *      This flag was introduced with SMBv3.0.2, and must be clear (0)
 *      in earlier SMB2/3 dialects.  For SMBv3.0.2 and above, this flag
 *      indicates that the share is hosted on a scale-out cluster, and that
 *      some cluster nodes provide better access to the share than others.
 *      In addition, the cluster node or nodes providing improved access
 *      may change while the share is in use.
 *      @todo Research this feature and provide a better explanation.
 *
 * @var SMB2_SHARE_CAP_REDIRECT_TO_OWNER
 *      This flag was introduced in SMBv3.1.1 and must be clear (0) in
 *      earlier dialects.  For SMBv3.1.1 and above, this flag indicates
 *      that the share is capable of performing synchronous redirect by
 *      replying to a Tree Connect Request with a Share Redirect Error
 *      Context Response.
 *      @see <a
 *        href="@msdocs/ms-smb2/f3073a8b-9f0f-47c0-91e5-ec3be9a49f37">[MS-SMB2;
 *        2.2.2.2.2]: Share Redirect Error Context Response</a>
 *      @todo Research this feature and provide a better explanation.
 *
 * @var SMB2_SHARE_CAP_MASK
 *      Bitmask to indicate all currently defined Share Capabilities bits.
 */
typedef enum
  {
  SMB2_SHARE_CAP_DFS                      = 0x00000008,
  SMB2_SHARE_CAP_CONTINUOUS_AVAILABILITY  = 0x00000010,
  SMB2_SHARE_CAP_SCALEOUT                 = 0x00000020,
  SMB2_SHARE_CAP_CLUSTER                  = 0x00000040,
  SMB2_SHARE_CAP_ASYMMETRIC               = 0x00000080,
  SMB2_SHARE_CAP_REDIRECT_TO_OWNER        = 0x00000100,
  SMB2_SHARE_CAP_MASK                     = 0x000001F8
  } SMB2_tcShareCaps;


/* -------------------------------------------------------------------------- **
 * Typedefs
 */

/**
 * @struct  smb2_TreeConnReq
 * @brief   SMB2/3 Tree Connect Request message structure.
 * @details
 *  Fundamentally, the Tree Connect Request consists of a share path.
 *  That's it.  Everything else is fluff, and much of the fluff was
 *  introduced with SMBv311.
 *
 * @var smb2_TreeConnReq::StructureSize
 *      The \c StructureSize field for this structure.  The \c StructureSize
 *      is (almost) always the size of the fixed portion of the message body
 *      plus one \e if there is a variable-length portion (a \c Buffer).  In
 *      this case, the \c StructureSize must be 9 to indicate the eight-byte
 *      length of the structure plus the variable \c Buffer.
 *
 * @var smb2_TreeConnReq::Flags
 *      Added in SMBv311, the flags field is used by the client to indicate
 *      support for advanced features and/or the presence of an additional
 *      data blob.  See \c #SMB2_tcFlags.
 *
 * @var smb2_TreeConnReq::Reserved
 *      For pre-3.1.1 dialects this field (which occupies the same position
 *      as the \c Flags field) is reserved and must be zero.
 *
 * @var smb2_TreeConnReq::PathOffset
 *      The offset, relative to the start of the header, of the share path
 *      within the \c Buffer blob.
 *
 * @var smb2_TreeConnReq::PathLength
 *      The length of the share path within the \c Buffer blob.  The share
 *      path is a length-delimited Unicode string in UTF-16LE format.
 *
 * @var smb2_TreeConnReq::Buffer
 *      This variable length field contains the share path or, optionally,
 *      an SMBv3.1.1 Tree Connect Request Extension.  If present, the
 *      Extension will begin at the first byte of the Buffer (at offset
 *      \c #SMB2_TREECONN_REQ_SIZE relative to the end of the header).
 */
typedef struct
  {
  uint16_t  StructureSize;  /**< Required message body size field.  */
  union
    {
    uint16_t Flags;         /**< Flags field for SMBv311 and above. */
    uint8_t  Reserved[2];   /**< Dialects < 3.1.1: Reserved-MB0.    */
    };
  uint16_t PathOffset;      /**< Offset of the share path string.   */
  uint16_t PathLength;      /**< Length of the share path string.   */
  uint8_t *Buffer;          /**< Data Blob.                         */
  } smb2_TreeConnReq;

/**
 * @struct  smb2_TreeConnExt
 * @brief   SMB2/3 Tree Connect Request Extension structure.
 * @details
 *  The Tree Connect Extension block was added in SMBv3.1.1.  It provides
 *  a framework for supporting a Context List as part of the Tree Connect
 *  Request message in addition to the variable-length share path.
 *
 * @see \c #SMB2_TREE_CONNECT_FLAG_EXTENSION_PRESENT
 *
 * @par Commentary 🚩
 * @parblock
 *  From a protocol perspective, there is no good reason that the
 *  \c TreeConnectContextOffset field needs to be a full 32 bits when 16
 *  bits would be well more than sufficient.  Similarly, ten reserved bytes
 *  seems a bit excessive.  These two oddities suggest that the Tree Connect
 *  Request Extension is an internal Windows structure thrown onto the wire
 *  without due consideration.
 *
 *  This is entropy.  It's how a nominally workable protocol degrades over
 *  time, which is what happened to SMB1.  Corner cases and oddities creep
 *  in, making it harder to adapt to different operating environments and
 *  bloating the overall system until it became too unwieldy to maintain
 *  even in its "native" environment.  It's the <a
 *  href="http://ubiqx.org/cifs/SMB.html#SMB.11">Winchester Mystery
 *  House</a> all over again.
 *
 *  See also: \c #smb2_ErrShareRedirCtx
 * @endparblock
 */
typedef struct
  {
  uint32_t  TreeConnectContextOffset; /**< Context offset within the message. */
  uint16_t  TreeConnectContextCount;  /**< The number of included contexts.   */
  uint8_t   Reserved[10];             /**< Reserved-MB0.                      */
  uint8_t  *PathName;                 /**< Pointer to the Share Path.         */
  uint8_t  *TreeConnectContextList;   /**< Pointer to the Context Lists.      */
  } smb2_TreeConnExt;

/**
 * @struct  smb2_TreeConnResp
 * @brief   SMB2/3 Tree Connect Response message structure.
 * @details The Tree Connect Response message provides oodles of information
 *          about the share, including an access mask that places an upper
 *          bound on the permissions that will be granted on CREATE.
 * @see <a href="@msdocs/ms-smb2/dd34e26c-a75e-47fa-aab2-6efc27502e96">[MS-SMB2;
 *      2.2.10]: SMB2 TREE_CONNECT Response</a>
 *
 * @var smb2_TreeConnResp::ShareType
 *      See \c #SMB2_tcShareType.
 * @var smb2_TreeConnResp::ShareFlags
 *      See \c #SMB2_tcShareFlags.
 * @var smb2_TreeConnResp::Capabilities
 *      See \c #SMB2_tcShareCaps.
 * @var smb2_TreeConnResp::MaximalAccess
 *      An SMB2 Access Mask.\n
 *      Access masks are usually associated with filesystem objects like
 *      files and directories.  In the [MS-SMB2] specification, readers are
 *      referred to the section covering <b>SMB2 Create</b> for information
 *      on access mask encoding.
 *      @see <a
 *        href="@msdocs/ms-smb2/b3af3aaf-9271-4419-b326-eba0341df7d2">[MS-SMB2;
 *        2.2.13.1]: SMB2 Access Mask Encoding</a>
 */
typedef struct
  {
  uint16_t  StructureSize;  /**< Required message body size field.        */
  uint8_t   ShareType;      /**< Disk, Named Pipe, or Printer share.      */
  uint8_t   Reserved;       /**< One byte; reserved-MB0.                  */
  uint32_t  ShareFlags;     /**< Share properties.                        */
  uint32_t  Capabilities;   /**< Share capabilities.                      */
  uint32_t  MaximalAccess;  /**< Maximum access rights to shared objects. */
  } smb2_TreeConnResp;

/**
 * @typedef smb2_TreeDiscReq
 * @brief   SMB2/3 Tree Disconnect Request message structure.
 * @details This maps directly to a \c #smb2_BaseMsg.  The important fields
 *          of the Tree Disconnect Request are carried in the header.
 * @see     \c #smb2_BaseMsg
 */
typedef smb2_BaseMsg smb2_TreeDiscReq;

/**
 * @typedef smb2_TreeDiscResp
 * @brief   SMB2/3 Tree Disconnect Response message structure.
 * @details This maps directly to a \c #smb2_BaseMsg.  The Tree Disconnect
 *          Response message is used to return a #STATUS_SUCCESS result.
 *          Errors, of course, are returned using an Error Response (see
 *          \c smb2_errorResp.h).
 * @see     \c #smb2_BaseMsg
 */
typedef smb2_BaseMsg smb2_TreeDiscResp;


/* -------------------------------------------------------------------------- **
 * Function Prototypes
 */

int smb2_parseTreeConnReq( uint16_t          const dialect,
                           uint8_t          *const msg,
                           size_t            const msgLen,
                           smb2_TreeConnReq *const tConnReq );

int smb2_packTreeConnReq( uint16_t          const dialect,
                          smb2_TreeConnReq *const tConnReq,
                          uint8_t          *const bufr,
                          uint32_t          const bSize );

int smb2_parseTreeConnResp( uint16_t           const dialect,
                            uint8_t           *const msg,
                            size_t             const msgLen,
                            smb2_TreeConnResp *const tConnResp );

int smb2_packTreeConnResp( uint16_t           const dialect,
                           smb2_TreeConnResp *const tConnResp,
                           uint8_t           *const bufr,
                           uint32_t           const bSize );

int smb2_parseTreeDiscReq( uint16_t          const dialect,
                           uint8_t          *const msg,
                           size_t            const msgLen,
                           smb2_TreeDiscReq *const tDiscReq );

int smb2_packTreeDiscReq( uint16_t          const dialect,
                          smb2_TreeDiscReq *const tDiscReq,
                          uint8_t          *const bufr,
                          uint32_t          const bSize );

int smb2_parseTreeDiscResp( uint16_t           const dialect,
                            uint8_t           *const msg,
                            size_t             const msgLen,
                            smb2_TreeDiscResp *const tDiscResp );

int smb2_packTreeDiscResp( uint16_t           const dialect,
                           smb2_TreeDiscResp *const tDiscResp,
                           uint8_t           *const bufr,
                           uint32_t           const bSize );


/* ============================ smb2_treeconn.h ============================= */
#endif /* SMB2_TREECONN_H */
