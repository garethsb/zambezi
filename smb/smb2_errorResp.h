#ifndef SMB2_ERRORRESP_H
#define SMB2_ERRORRESP_H
/* ========================================================================== **
 *                              smb2_errorResp.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Marshall/unmarshall SMB2/3 Error Response messages.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb2_errorResp.h; 2020-11-27 12:35:38 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file    smb2_errorResp.h
 * @author  Christopher R. Hertel
 * @brief   Format and deconstruct SMB2/3 Error Response messages.
 * @date    15-Sep-2020
 * @version \$Id: smb2_errorResp.h; 2020-11-27 12:35:38 -0600; crh$
 * @copyright Copyright (C) 2020 by Christopher R. Hertel
 *
 * @details
 *  The Error response message can be sent by the server to the client as a
 *  reply to just about any command.  It is, of course, only sent when an
 *  error condition is raised.  Error conditions, however, may include
 *  notification that additional processing or resources are required in
 *  order to complete the requested action.
 *
 * \b Notes
 *  - Error response messages are sent in reply to client requests that have
 *    generated a server-side error of some kind.
 *  - The error code is returned in the \c Status field of the SMB2 Header.
 *    It is \e not contained within the error response structure.
 *  - The presence of a non-zero error code in the Status field is the clue
 *    that the response message is an Error Response.
 *
 * @see <a
 *  href="@msdocs/ms-smb2/d4da8b67-c180-47e3-ba7a-d24214ac4aaa">[MS-SMB2;
 *  2.2.2]: SMB2 ERROR Response</a>
 * @see <a
 *  href="@msdocs/ms-smb2/9ff8837c-c4f7-452b-9272-8818a119dae9">[MS-SMB2;
 *  2.2.2.1]: SMB2 ERROR Context Response</a>
 * @see <a
 *  href="@msdocs/ms-erref/1bc92ddf-b79e-413c-bbaa-99a5281a6c90">[MS-ERREF]:
 *  Windows Error Codes</a>
 *
 * @todo
 *  Add full support for the Share Redirect Error Context.
 *  - The structures are in place.
 *  - Packing and parsing of the structures still needs to be written.
 *  - The \c IPAddrMoveList and \c ResourceName substructures need
 *    packing/parsing support as well.
 *  .
 *  The Share Redirect Error Context is really ugly, IMNSHO.
 *
 * @todo
 *  Higher-level functions, such as as those that can parse/pack whole
 *  contexts out of a ContextList, should be provided.  This module is
 *  quite rudimentary at present.
 *
 * @todo
 *  Code review.
 */


/* -------------------------------------------------------------------------- **
 * Defined Constants
 */

/**
 * @def     SMB2_ERROR_RESP_SIZE
 * @brief   The byte length of the fixed portion of the Error Response
 *          message.
 * @def     SMB2_ERROR_RESP_STRUCT_SIZE
 * @brief   The StructureSize for the Error Response.
 * @details This value is one more than #SMB2_ERROR_RESP_SIZE to indicate
 *          that a variable length data blob may be appended.
 *
 * @def     SMB2_SYM_LINK_ERROR_TAG
 * @brief   Required value for the \c SymLinkErrorTag field of the Symbolic
 *          Link Error Response Context.
 * @def     SMB2_SYM_LINK_REPARSE_TAG
 * @brief   Required value for the \c ReparseTag field of the Symbolic Link
 *          Error Response Context.
 */

#define SMB2_ERROR_RESP_SIZE        8
#define SMB2_ERROR_RESP_STRUCT_SIZE 9

#define SMB2_SYM_LINK_ERROR_TAG   0x4C4D5953
#define SMB2_SYM_LINK_REPARSE_TAG 0xA000000C


/* -------------------------------------------------------------------------- **
 * Enumerated Constants
 */

/**
 * @enum  smb2_ErrCtxErrorId
 * @brief Obscure identifier used internally by Windows, probably.
 * @see <a
 *  href="@msdocs/ms-smb2/9ff8837c-c4f7-452b-9272-8818a119dae9">[MS-SMB2;
 *  2.2.2.1]: SMB2 ERROR Context Response</a>
 *
 * @var SMB2_ERROR_ID_DEFAULT
 *  The default error identifier.  All error contexts in SMB2/3 use this
 *  error Id unless otherwise specified.
 * @var SMB2_ERROR_ID_SHARE_REDIRECT
 *  Otherwise specified.  This is a special-case error Id used only by the
 *  Share Redirect Error Context.  See \c #smb2_ErrShareRedirCtx.
 */
typedef enum
  {
  SMB2_ERROR_ID_DEFAULT        = 0x00000000,
  SMB2_ERROR_ID_SHARE_REDIRECT = 0x72645253
  } smb2_ErrCtxErrorId;

/**
 * @enum smb2_SymLinkCtxFlags
 *  Indicate whether the substitute name is a relative or absolute pathname.
 * @see <a href="@msdocs/ms-smb2/f15ae37d-a787-4bf2-9940-025a8c9c0022">[MS-SMB2;
 *  2.2.2.2.1]: Symbolic Link Error Response</a>
 * @see <a href="@msdocs/ms-smb2/a8da655c-8b0b-415a-b726-16dc33fa5827">[MS-SMB2;
 *  2.2.2.2.1.1]: Handling the Symbolic Link Error Response</a>
 * @var SMB2_SYMLINK_FLAG_ABSOLUTE
 *  The substitute name is absolute.
 * @var SMB2_SYMLINK_FLAG_RELATIVE
 *  The substitute name is relative to the directory in which the symbolic
 *  link was encountered.
 */
typedef enum
  {
  SMB2_SYMLINK_FLAG_ABSOLUTE = 0x00000000,
  SMB2_SYMLINK_FLAG_RELATIVE = 0x00000001
  } smb2_SymLinkCtxFlags;

/**
 * @enum smb2_IPAddrMoveType
 *  Identify whether a given IP address is IPv4 or IPv6.
 *
 * @see <a href="@msdocs/ms-smb2/0d2e8ab8-8261-42d8-a90b-bdcb1fb40515">[MS-SMB2;
 *  2.2.2.2.2.1]: MOVE_DST_IPADDR structure</a>
 */
typedef enum
  {
  MOVE_DST_IPADDR_V4 = 0x00000001,    /**< Interpret \c IPAddress as IPv4.  */
  MOVE_DST_IPADDR_V6 = 0x00000002     /**< Interpret \c IPAddress as IPv6.  */
  } smb2_IPAddrMoveType;


/* -------------------------------------------------------------------------- **
 * Typedefs
 */

/**
 * @struct  smb2_ErrResp
 * @brief   General structure of an Error Response message.
 * @details
 *  The Error Response message is a top-level SMB2/3 message, like any of
 *  the command request or response messages.  In theory, it may be returned
 *  in response to just about any command request.  In practice, several of
 *  the commands do not return an error.
 *
 *  The Error Response may only be sent if the \c Status field in the
 *  #smb2_Header is non-zero.  That is, this structure is only sent by
 *  the server when an error is being reported to the client.
 *
 * @var smb2_ErrResp::StructureSize
 *  Must be 9 (nine).
 * @var smb2_ErrResp::ErrorContextCount
 *  For dialects prior to 3.1.1, this field is reserved and must be zero.
 *  For 3.1.1 (and above), this field indicates the number of contexts
 *  in the list of contexts contained within \c ErrorData.
 * @var smb2_ErrResp::Reserved
 *  Reserved, must be zero.
 * @var smb2_ErrResp::ByteCount
 *  The byte length of \c ErrorData.
 * @var smb2_ErrResp::ErrorData
 *  If \c ByteCount is not zero, the following rules apply:
 *  - In dialects prior to 3.1.1, this field will contain an Error Context.
 *  - In 3.1.1 and above, this field will contain an array of one or more
 *    contexts, each of which will be prefixed by an Error Context header.
 *    See \c #smb2_ErrCtxResp.
 */
typedef struct
  {
  uint16_t  StructureSize;
  uint8_t   ErrorContextCount;
  uint8_t   Reserved;
  uint32_t  ByteCount;
  uint8_t  *ErrorData;
  } smb2_ErrResp;

/**
 * @struct  smb2_ErrCtxResp
 * @brief   The Error Context header structure.
 * @details
 *  Staring with SMBv3.1.1, Error Response messages may have an array of
 *  zero or more contexts appended to the message.  This context list
 *  replaces the simpler structure provided in earlier dialects.
 *
 *  Each context in the array is preceeded by a header that indicates the
 *  amount of raw data (bytes) consumed by the \c ErrorContextData and
 *  the type of the context.
 *
 *  In practical terms, the contexts currently defined are returned for
 *  different errors in response to different commands.  There is no reason
 *  to create an array greater in length than 1.  This, however, may change
 *  in the future.
 *
 * @var smb2_ErrCtxResp::ErrorDataLength
 *  The byte length of \c ErrorContextData.
 * @var smb2_ErrCtxResp::ErrorId
 *  An identifier to indicate the type of error context being returned.
 *  See \c #smb2_ErrCtxErrorId
 * @var smb2_ErrCtxResp::ErrorContextData
 *  The data blob containing the Error Context.
 */
typedef struct
  {
  uint32_t  ErrorDataLength;
  uint32_t  ErrorId;
  uint8_t  *ErrorContextData;
  } smb2_ErrCtxResp;

/**
 * @struct  smb2_ErrBufferCtx
 * @brief   A Buffer Too Small Context structure.
 * @details
 *  This is the simplest Error Context.  The return value indicates the
 *  minimum size for a required buffer.  This context is returned along
 *  with a \c #STATUS_BUFFER_TOO_SMALL error code.
 * @see <a href="@msdocs/ms-smb2/7722d25d-cec7-4baf-8343-9ef84f48a52c">[MS-SMB2;
 *  2.2.2.2]: ErrorData format</a>
 */
typedef struct
  {
  uint32_t  minBuffer;  /**< Minimum required buffer length, in bytes.  */
  } smb2_ErrBufferCtx;

/**
 * @struct  smb2_ErrSymLinkCtx
 * @brief   Symbolic Link Error Response Context structure.
 * @details
 *  This context may be returned in reply to an SMB2 Create request (which
 *  can both open existing objects and create new ones).  If an element of
 *  the path is a symbolic link, the Create can return an error code of
 *  \c #STATUS_STOPPED_ON_SYMLINK.  This context will be appended to the
 *  Error Response to provide an updated path for the client to traverse.
 *
 * @see <a href="@msdocs/ms-smb2/f15ae37d-a787-4bf2-9940-025a8c9c0022">[MS-SMB2;
 *  2.2.2.2.1]: Symbolic Link Error Response</a>
 * @see <a href="@msdocs/ms-smb2/a8da655c-8b0b-415a-b726-16dc33fa5827">[MS-SMB2;
 *  2.2.2.2.1.1]: Handling the Symbolic Link Error Response</a>
 *
 * @var smb2_ErrSymLinkCtx::SymLinkLength
 *  The overall length of this Symbolic Link Context object \e excluding
 *  the \c SymLinkLength field itself.
 * @var smb2_ErrSymLinkCtx::SymLinkErrorTag
 *  Reserved, must be \c 0x4C4D5953.
 * @var smb2_ErrSymLinkCtx::ReparseTag
 *  Reserved, must be \c 0xA000000C.
 * @var smb2_ErrSymLinkCtx::ReparseDataLength
 *  The byte length of a portion of the SymLink Context.  The calculation
 *  of this value and how it is used are explained in [MS-SMB2; 2.2.2.2.1]
 *  and [MS-SMB2; 2.2.2.2.1.1].
 * @var smb2_ErrSymLinkCtx::UnparsedPathLength
 *  The byte length of the unparsed portion of the path.
 * @var smb2_ErrSymLinkCtx::SubstituteNameOffset
 *  The offset into the \c PathBuffer string blob at which the substitute
 *  name is located.
 * @var smb2_ErrSymLinkCtx::SubstituteNameLength
 *  The byte length of the substitute name.
 * @var smb2_ErrSymLinkCtx::PrintNameOffset
 *  The offset into the \c PathBuffer string blob at which the
 *  "user-friendly" name (if any) is located.
 * @var smb2_ErrSymLinkCtx::PrintNameLength
 *  The byte length of the "user-friendly" name.
 * @var smb2_ErrSymLinkCtx::Flags
 *  Indicates absolute or relative paths.  See #smb2_SymLinkCtxFlags.
 * @var smb2_ErrSymLinkCtx::PathBuffer
 *  A string blob.  The strings contained in this buffer are in Unicode
 *  format.
 */
typedef struct
  {
  uint32_t  SymLinkLength;
  uint32_t  SymLinkErrorTag;
  uint32_t  ReparseTag;
  uint16_t  ReparseDataLength;
  uint16_t  UnparsedPathLength;
  uint16_t  SubstituteNameOffset;
  uint16_t  SubstituteNameLength;
  uint16_t  PrintNameOffset;
  uint16_t  PrintNameLength;
  uint32_t  Flags;
  uint8_t  *PathBuffer;
  } smb2_ErrSymLinkCtx;

/**
 * @struct  smb2_ErrShareRedirCtx
 * @brief   Share Redirect Error Context structure.
 * @details
 *  This Error Response Context was introduced with SMBv3.1.1.  Earlier
 *  dialects do not support this context.  In a clustered environment, this
 *  context may be sent in reply to a Tree Connect request.  It directs the
 *  client to connect to a different server node to access the requested
 *  share.  The returned error code must be \c #STATUS_BAD_NETWORK_NAME.
 *
 * @par Commentary 🚩
 *  This is probably an internal Windows structure that was thrown onto the
 *  wire without care or consideration.  The structure is awkward and does
 *  not comply with SMB2/3 norms.  The documentation is unclear, poorly
 *  worded, and lacking in explanations or useful references.  Bug reports
 *  have been filed with \c DocHelp.
 *
 * @see <a href="@msdocs/ms-smb2/652e0c14-5014-4470-999d-b174d7b2da87">[MS-SMB2;
 *  3.3.5.7]: Receiving an SMB2 TREE_CONNECT Request</a>
 *
 * @var smb2_ErrShareRedirCtx::ShareRedirSize
 *  The size, in bytes, of the Error Share Redirect Context.  This includes
 *  both of the variable-length fields (\c IPAddrMoveList and
 *  \c ResourceName).
 * @note
 *  This field is given as \c StructureSize in the documentation, but its
 *  syntax and semantics do not match any other instance of \c StructureSize
 *  anywhere in the documetation.  All other instances are USHORT (uint16_t)
 *  fields with pre-defined values.  We have renamed this field to comply
 *  with the example of \c SymLinkLength given in the Symbolic Link Error
 *  Response.
 *
 * @var smb2_ErrShareRedirCtx::NotificationType
 *  Reserved, must be 3.
 * @note
 *  This field is probably a hangover from an internal Windows structure
 *  that should, in all likelihood, never be exposed on the wire.
 *
 * @var smb2_ErrShareRedirCtx::ResourceNameOffset
 *  The offset, relative to the start of the Error Share Redirect Context,
 *  at which the \c ResourceName field begins.
 *
 * @var smb2_ErrShareRedirCtx::ResourceNameLength
 *  The size, in bytes, of the \c ResourceName.
 *
 * @var smb2_ErrShareRedirCtx::Flags
 *  Reserved, must be zero.
 *
 * @var smb2_ErrShareRedirCtx::TargetType
 *  Reserved, must be zero.
 *
 * @var smb2_ErrShareRedirCtx::IPAddrCount
 *  The count of the number of IP Address entries in \c IPAddrMoveList.
 *
 * @var smb2_ErrShareRedirCtx::IPAddrMoveList
 *  A list of IPv4 and/or IPv6 addresses.  Each entry is prefixed with
 *  a \c Type that indicates whether it is IPv4 or IPv6.  See
 *  #smb2_ErrMoveDstIP.
 *
 * @var smb2_ErrShareRedirCtx::ResourceName
 *  A pointer to an additional data blob containing a Windows
 *  \c UNICODE_STRING structure.  See #smb2_ErrResourceNameStr
 */
typedef struct
  {
  uint32_t  ShareRedirSize;
  uint32_t  NotificationType;
  uint32_t  ResourceNameOffset;
  uint32_t  ResourceNameLength;
  uint16_t  Flags;
  uint16_t  TargetType;
  uint32_t  IPAddrCount;
  uint8_t  *IPAddrMoveList;
  uint8_t  *ResourceName;
  } smb2_ErrShareRedirCtx;

/**
 * @struct  smb2_ErrMoveDstIP
 * @brief   IP Address List Element.
 * @details
 *  The \c #smb2_ErrShareRedirCtx::IPAddrMoveList structure is an array of
 *  IP addresses.  These may be either IPv4 or IPv6 addresses.  An IPv4
 *  address will be transmitted using first four bytes of the \c IPAddress
 *  field; the remainder are reserved and should be zero filled.  An IPv6
 *  address will use the entire \c IPAddress field.
 *
 * @var smb2_ErrMoveDstIP::Type
 *  Indicates whether the \c IPAddress field contains an IPv4 or IPv6
 *  address.  See #smb2_IPAddrMoveType.
 * @var smb2_ErrMoveDstIP::Reserved
 *  Four bytes; Reserved-MB0.
 * @var smb2_ErrMoveDstIP::IPAddress
 *  An IPv4 or IPv6 address in network byte order.  If the \c Type is IPv4,
 *  only the first four bytes of \c IPAddress are used; the remainder is
 *  reserved and must be zero.  If the \c Type is IPv6, then the entire 16
 *  bytes are used.
 */
typedef struct
  {
  uint32_t  Type;
  uint8_t   Reserved[4];
  uint8_t   IPAddress[16];
  } smb2_ErrMoveDstIP;

/**
 * @struct  smb2_ErrResourceNameStr
 * @brief   A Windows structure used to describe a Unicode string.
 * @see <a
 *   href="https://docs.microsoft.com/en-us/windows/win32/api/subauth/ns-subauth-unicode_string">
 *  (Windows) UNICODE_STRING structure</a>
 * @details
 *  This format is sometimes referred to in Windows documentation as a
 *  "counted Unicode string".
 *
 * @attention
 *  This is a Windows-internal structure that should not have been exposed
 *  on the wire.
 *
 * @var smb2_ErrResourceNameStr::strLength
 *  The length, <i>in bytes</i>, of the Unicode string.  This value
 *  \b must be a multiple of two.
 *
 * @var smb2_ErrResourceNameStr::maxLength
 *  This value is used, in Windows, to indicate the number of bytes that
 *  were allocated for the Unicode string.  If this value is greater than
 *  \c strLength, the additional bytes may be present as padding. Caution
 *  is advised.
 *
 * @var smb2_ErrResourceNameStr::uStr
 *  The Unicode string.  Windows uses UTF-16LE encoding, so each
 *  character is two-bytes wide allowing for 64K characters.  Additional
 *  characters can also be represented.
 * @see <a href=
 *  "https://docs.microsoft.com/en-us/windows/win32/intl/unicode">(Windows)
 *  Unicode</a>
 */
typedef struct
  {
  uint16_t  strLength;
  uint16_t  maxLength;
  uint16_t *uStr;
  } smb2_ErrResourceNameStr;


/* -------------------------------------------------------------------------- **
 * Function Prototypes
 */
int smb2_parseErrorResp( uint16_t      const dialect,
                         uint8_t      *const msg,
                         size_t        const msgLen,
                         smb2_ErrResp *const errResp );

int smb2_packErrorResp( uint16_t      const dialect,
                        smb2_ErrResp *const errResp,
                        uint8_t      *const bufr,
                        uint32_t      const bSize );

int smb2_parseErrCtxResp( uint16_t         const dialect,
                          uint8_t         *const msg,
                          size_t           const msgLen,
                          smb2_ErrCtxResp *const ctxHdr );

int smb2_packErrCtxResp( uint16_t         const dialect,
                         smb2_ErrCtxResp *const ctxHdr,
                         uint8_t         *const bufr,
                         uint32_t         const bSize );

int smb2_parseBufferCtx( uint16_t           const dialect,
                         uint8_t           *const msg,
                         size_t             const msgLen,
                         smb2_ErrBufferCtx *const bufCtx );

int smb2_packBufferCtx( uint16_t           const dialect,
                        smb2_ErrBufferCtx *const bufCtx,
                        uint8_t           *const bufr,
                        uint32_t           const bSize );

int smb2_parseSymLinkCtx( uint16_t            const dialect,
                          uint8_t            *const msg,
                          size_t              const msgLen,
                          smb2_ErrSymLinkCtx *const sLinkCtx );

int smb2_packSymLinkCtx( uint16_t            const dialect,
                         smb2_ErrSymLinkCtx *const sLinkCtx,
                         uint8_t            *const bufr,
                         uint32_t            const bSize );


/* ============================ smb2_errorResp.h ============================ */
#endif /* SMB2_ERRORRESP_H */
