#ifndef SMB_NTSTATUS_H
#define SMB_NTSTATUS_H
/* ========================================================================== **
 *                               smb_ntstatus.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Windows NT Status Code support.
 *
 * Copyright (C) 2019 by Christopher R. Hertel
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *  To add a missing status code, please do the following:
 *    - Add it to the NTSTATUS enumerated type, below.  Status codes
 *      should be listed in numerical order.
 *    - Add it to the Doxygen documentation.  Again, please add the new
 *      listing in numerical order.
 *    - Update the switch() statement in smb_ntstatus.c.
 *    - Accept the thanks of your peers in the commuity.
 *
 * ========================================================================== **
 *//**
 * @file      smb_ntstatus.h
 * @author    Christopher R. Hertel
 * @date      18 Apr 2019
 * @version   \$Id: smb_ntstatus.h; 2020-09-15 18:49:56 -0500; crh$
 * @copyright Copyright (C) 2019 by Christopher R. Hertel
 * @brief     32-bit Windows NT (and, threfore, SMB2/SMB3) status codes.
 *
 * @details
 *  This module provides definitions for the set of #NTSTATUS codes
 *  that are known to be used by SMB clients and servers.  It also
 *  provides mappings from #NTSTATUS code values to their respective
 *  names and error messages.\n
 *
 *  For example, #STATUS_NO_SUCH_USER has a value of \c 0xC0000064.
 *  That value maps to two strings:
 *  - \c name : "STATUS_NO_SUCH_USER"
 *  - \c msg  : "The specified account does not exist."
 *
 *  #NTSTATUS codes are composed of several subfields; this module
 *  provides tools to deconstruct an #NTSTATUS code.  This feature
 *  is probably not very useful, but it was easy enough to code up.
 *
 * @see <a
 *  href="@msdocs/ms-erref/1bc92ddf-b79e-413c-bbaa-99a5281a6c90">[MS-ERREF]:
 *  Windows Error Codes</a>
 */

#include <stdint.h>   /* Standard integer types.  */


/* -------------------------------------------------------------------------- **
 * Macros
 */

/** Extract the severity level from an #NTSTATUS code.
 *
 * @param[in] nts   An #NTSTATUS code.
 *
 * @returns   A value in the range 0..3 indicating the severity of the
 *            status code: \code
 *                0 == Success  1 == Information  2 == Warning  3 == Error
 *            \endcode
 */
#define ntstatus_getSeverity( nts ) (((uint32_t)(nts) >> 30) & 0x03)


/* -------------------------------------------------------------------------- **
 * Enumerated Constants
 */

/**
 * @enum NTSTATUS
 * @details
 *  The #NTSTATUS type is defined in <a
 *  href="@msdocs/ms-erref/87fba13e-bf06-450e-83b1-9241dc81e781">[MS-ERREF;
 *  2.3]</a>.  #NTSTATUS codes are 32-bit unsigned integers.  They can be
 *  further broken down into five subfields, as follows:
 *  - Severity    (2 bits)
 *  - Customer    (1 bit)
 *  - N (reserved; 1 bit)
 *  - Facility   (12 bits)
 *  - Code       (16 bits)
 *
 *  See <a
 *  href="@msdocs/ms-erref/596a1078-e883-4972-9bbc-49e60bebca55">[MS-ERREF;
 *  2.3.1]</a> for the official list of Windows #NTSTATUS values and their
 *  respective English language error messages.
 *
 * @var STATUS_SUCCESS
 *      \c 0x00000000 \n The operation completed successfully.
 * @var STATUS_PENDING
 *      \c 0x00000103 \n The operation that was requested is pending
 *      completion.
 * @var STATUS_REPARSE
 *      \c 0x00000104 \n A reparse should be performed by the Object Manager
 *      because the name of the file resulted in a symbolic link.
 * @var STATUS_NOTIFY_CLEANUP
 *      \c 0x0000010B \n Indicates that a notify change request has been
 *      completed due to closing the handle that made the notify change
 *      request.
 * @var STATUS_NOTIFY_ENUM_DIR
 *      \c 0x0000010C \n Indicates that a notify change request is being
 *      completed and that the information is not being returned in the
 *      caller's buffer. The caller now needs to enumerate the files to find
 *      the changes.
 * @var STATUS_BUFFER_OVERFLOW
 *      \c 0x80000005 \n Buffer Overflow; The data was too large to fit into
 *      the specified buffer.
 * @var STATUS_NO_MORE_FILES
 *      \c 0x80000006 \n No more files were found which match the file
 *      specification.
 * @var STATUS_EA_LIST_INCONSISTENT
 *      \c 0x80000014 \n The extended attribute (EA) list is inconsistent.
 * @var STATUS_NO_MORE_ENTRIES
 *      \c 0x8000001A \n No more entries are available from an enumeration
 *      operation.
 * @var STATUS_STOPPED_ON_SYMLINK
 *      \c 0x8000002D \n The create operation stopped after reaching a
 *      symbolic link.
 * @var STATUS_UNSUCCESSFUL
 *      \c 0xC0000001 \n Operation Failed; The requested operation was
 *      unsuccessful.
 * @var STATUS_INVALID_INFO_CLASS
 *      \c 0xC0000003 \n Invalid Parameter; The specified information class is
 *      not a valid information class for the specified object.
 * @var STATUS_INFO_LENGTH_MISMATCH
 *      \c 0xC0000004 \n The specified information record length does not
 *      match the length that is required for the specified information class.
 * @var STATUS_INVALID_HANDLE
 *      \c 0xC0000008 \n An invalid HANDLE was specified.
 * @var STATUS_INVALID_PARAMETER
 *      \c 0xC000000D \n An invalid parameter was passed to a service or
 *      function.
 * @var STATUS_NO_SUCH_FILE
 *      \c 0xC000000F \n File Not Found; The file does not exist.
 * @var STATUS_INVALID_DEVICE_REQUEST
 *      \c 0xC0000010 \n The specified request is not a valid operation for
 *      the target device.
 * @var STATUS_END_OF_FILE
 *      \c 0xC0000011 \n The end-of-file marker has been reached. There is no
 *      valid data in the file beyond this marker.
 * @var STATUS_MORE_PROCESSING_REQUIRED
 *      \c 0xC0000016 \n Still Busy; The specified I/O request packet (IRP)
 *      cannot be disposed of because the I/O operation is not complete.
 * @var STATUS_NO_MEMORY
 *      \c 0xC0000017 \n Insufficient Quota; Not enough virtual memory or
 *      paging file quota is available to complete the specified operation.
 * @var STATUS_ACCESS_DENIED
 *      \c 0xC0000022 \n A process has requested access to an object but has
 *      not been granted those access rights.
 * @var STATUS_BUFFER_TOO_SMALL
 *      \c 0xC0000023 \n The buffer is too small to contain the entry.  No
 *      information has been written to the buffer.
 * @var STATUS_OBJECT_NAME_INVALID
 *      \c 0xC0000033 \n The object name is invalid.
 * @var STATUS_OBJECT_NAME_NOT_FOUND
 *      \c 0xC0000034 \n The object name is not found.
 * @var STATUS_OBJECT_NAME_COLLISION
 *      \c 0xC0000035 \n The object name already exists.
 * @var STATUS_EAS_NOT_SUPPORTED
 *      \c 0xC000004F \n An operation involving EAs failed because the file
 *      system does not support EAs.
 * @var STATUS_NONEXISTENT_EA_ENTRY
 *      \c 0xC0000051 \n An EA operation failed because the name or EA index
 *      is invalid.
 * @var STATUS_FILE_LOCK_CONFLICT
 *      \c 0xC0000054 \n A requested read/write cannot be granted due to a
 *      conflicting file lock.
 * @var STATUS_LOCK_NOT_GRANTED
 *      \c 0xC0000055 \n A requested file lock cannot be granted due to other
 *      existing locks.
 * @var STATUS_NO_SUCH_LOGON_SESSION
 *      \c 0xC000005F \n A specified logon session does not exist. It may
 *      already have been terminated.
 * @var STATUS_NO_SUCH_USER
 *      \c 0xC0000064 \n The specified account does not exist.
 * @var STATUS_WRONG_PASSWORD
 *      \c 0xC000006A \n When trying to update a password, this return status
 *      indicates that the value provided as the current password is not
 *      correct.
 * @var STATUS_PASSWORD_RESTRICTION
 *      \c 0xC000006C \n When trying to update a password, this status
 *      indicates that some password update rule has been violated.  For
 *      example, the password may not meet length criteria.
 * @var STATUS_LOGON_FAILURE
 *      \c 0xC000006D \n The attempted logon is invalid.  This is either due
 *      to a bad username or authentication information.
 * @var STATUS_INVALID_LOGON_HOURS
 *      \c 0xC000006F \n The user account has time restrictions and may not be
 *      logged onto at this time.
 * @var STATUS_INVALID_WORKSTATION
 *      \c 0xC0000070 \n The user account is restricted so that it may not be
 *      used to log on from the source workstation.
 * @var STATUS_PASSWORD_EXPIRED
 *      \c 0xC0000071 \n The user account password has expired.
 * @var STATUS_NONE_MAPPED
 *      \c 0xC0000073 \n None of the information to be translated has been
 *      translated.
 * @var STATUS_NO_TOKEN
 *      \c 0xC000007C \n An attempt was made to reference a token that does
 *      not exist.  This is typically done by referencing the token that is
 *      associated with a thread when the thread is not impersonating a
 *      client.
 * @var STATUS_RANGE_NOT_LOCKED
 *      \c 0xC000007E \n The range specified in NtUnlockFile was not locked.
 * @var STATUS_DISK_FULL
 *      \c 0xC000007F \n An operation failed because the disk was full.
 * @var STATUS_INSUFFICIENT_RESOURCES
 *      \c 0xC000009A \n Insufficient system resources exist to complete the
 *      API.
 * @var STATUS_IO_TIMEOUT
 *      \c 0xC00000B5 \n Device Timeout; The specified I/O operation was not
 *      completed before the time-out period expired.
 * @var STATUS_FILE_FORCED_CLOSED
 *      \c 0xC00000B6 \n The specified file has been closed by another
 *      process.
 * @var STATUS_FILE_IS_A_DIRECTORY
 *      \c 0xC00000BA \n The file that was specified as a target is a
 *      directory, and the caller specified that it could be anything but a
 *      directory.
 * @var STATUS_NOT_SUPPORTED
 *      \c 0xC00000BB \n The request is not supported.
 * @var STATUS_INVALID_NETWORK_RESPONSE
 *      \c 0xC00000C3 \n The network responded incorrectly.
 * @var STATUS_NETWORK_NAME_DELETED
 *      \c 0xC00000C9 \n The network name was deleted.
 * @var STATUS_BAD_NETWORK_NAME
 *      \c 0xC00000CC \n The share name cannot be found on the server.
 * @var STATUS_REQUEST_NOT_ACCEPTED
 *      \c 0xC00000D0 \n No more connections can be made to this remote
 *      computer at this time because the computer has already accepted the
 *      maximum number of connections.
 * @var STATUS_NO_SUCH_DOMAIN
 *      \c 0xC00000DF \n The specified domain did not exist.
 * @var STATUS_INVALID_OPLOCK_PROTOCOL
 *      \c 0xC00000E3 \n An error status returned when an invalid
 *      opportunistic lock (oplock) acknowledgment is received by a file
 *      system.
 * @var STATUS_INTERNAL_ERROR
 *      \c 0xC00000E5 \n An internal error occurred.
 * @var STATUS_FILE_CORRUPT_ERROR
 *      \c 0xC0000102 \n Corrupt File; The file or directory is corrupt and
 *      unreadable.
 * @var STATUS_NOT_A_DIRECTORY
 *      \c 0xC0000103 \n A requested opened file is not a directory.
 * @var STATUS_CANCELLED
 *      \c 0xC0000120 \n The I/O request was canceled.
 * @var STATUS_FILE_CLOSED
 *      \c 0xC0000128 \n An I/O request other than close and several other
 *      special case operations was attempted using a file object that had
 *      already been closed.
 * @var STATUS_PIPE_BROKEN
 *      \c 0xC000014B \n The pipe operation has failed because the other end
 *      of the pipe has been closed.
 * @var STATUS_LOGON_TYPE_NOT_GRANTED
 *      \c 0xC000015B \n A user has requested a type of logon (for example,
 *      interactive or network) that has not been granted. An administrator
 *      has control over who may logon interactively and through the network.
 * @var STATUS_INVALID_DEVICE_STATE
 *      \c 0xC0000184 \n The device is not in a valid state to perform this
 *      request.
 * @var STATUS_TRUSTED_RELATIONSHIP_FAILURE
 *      \c 0xC000018D \n The logon request failed because the trust
 *      relationship between this workstation and the primary domain failed.
 * @var STATUS_TRUST_FAILURE
 *      \c 0xC0000190 \n The network logon failed. This may be because the
 *      validation authority cannot be reached.
 * @var STATUS_NETLOGON_NOT_STARTED
 *      \c 0xC0000192 \n An attempt was made to logon, but the NetLogon
 *      service was not started.
 * @var STATUS_FS_DRIVER_REQUIRED
 *      \c 0xC000019C \n A volume has been accessed for which a file system
 *      driver is required that has not yet been loaded.
 * @var STATUS_USER_SESSION_DELETED
 *      \c 0xC0000203 \n The remote user session has been deleted.
 * @var STATUS_CONNECTION_DISCONNECTED
 *      \c 0xC000020C \n The transport connection is now disconnected.
 * @var STATUS_PASSWORD_MUST_CHANGE
 *      \c 0xC0000224 \n The user password must be changed before logging on
 *      the first time.
 * @var STATUS_DUPLICATE_OBJECTID
 *      \c 0xC000022A \n The attempt to insert the ID in the index failed
 *      because the ID is already in the index.
 * @var STATUS_DOMAIN_CONTROLLER_NOT_FOUND
 *      \c 0xC0000233 \n A domain controller for this domain was not found.
 * @var STATUS_NETWORK_UNREACHABLE
 *      \c 0xC000023C \n The remote network is not reachable by the transport.
 * @var STATUS_VOLUME_DISMOUNTED
 *      \c 0xC000026E \n An operation was attempted to a volume after it was
 *      dismounted.
 * @var STATUS_PKINIT_NAME_MISMATCH
 *      \c 0xC00002F9 \n The client certificate does not contain a valid UPN,
 *      or does not match the client name in the logon request.
 * @var STATUS_PKINIT_FAILURE
 *      \c 0xC0000320 \n The Kerberos protocol encountered an error while
 *      validating the KDC certificate during smart card logon. There is more
 *      information in the system event log.
 * @var STATUS_NETWORK_SESSION_EXPIRED
 *      \c 0xC000035C \n The client session has expired; The client must re-
 *      authenticate to continue accessing the remote resources.
 * @var STATUS_SMARTCARD_WRONG_PIN
 *      \c 0xC0000380 \n An incorrect PIN was presented to the smart card.
 * @var STATUS_SMARTCARD_CARD_BLOCKED
 *      \c 0xC0000381 \n The smart card is blocked.
 * @var STATUS_SMARTCARD_NO_CARD
 *      \c 0xC0000383 \n No smart card is available.
 * @var STATUS_DOWNGRADE_DETECTED
 *      \c 0xC0000388 \n The system detected a possible attempt to compromise
 *      security.  Ensure that you can contact the server that authenticated
 *      you.
 * @var STATUS_PKINIT_CLIENT_FAILURE
 *      \c 0xC000038C \n The smart card certificate used for authentication
 *      was not trusted.  Contact your system administrator.
 * @var STATUS_SMARTCARD_SILENT_CONTEXT
 *      \c 0xC000038F \n The smart card provider could not perform the action
 *      because the context was acquired as silent.
 * @var STATUS_SERVER_UNAVAILABLE
 *      \c 0xC0000466 \n The file server is temporarily unavailable.
 * @var STATUS_FILE_NOT_AVAILABLE
 *      \c 0xC0000467 \n The file is temporarily unavailable.
 * @var STATUS_HASH_NOT_SUPPORTED
 *      \c 0xC000A100 \n Hash generation for the specified version and hash
 *      type is not enabled on server.
 * @var STATUS_HASH_NOT_PRESENT
 *      \c 0xC000A101 \n The hash requested is not present or not up to date
 *      with the current file contents.
 */

typedef enum
  {
  STATUS_SUCCESS                      = 0x00000000,
  STATUS_PENDING                      = 0x00000103,
  STATUS_REPARSE                      = 0x00000104,
  STATUS_NOTIFY_CLEANUP               = 0x0000010B,
  STATUS_NOTIFY_ENUM_DIR              = 0x0000010C,
  STATUS_BUFFER_OVERFLOW              = 0x80000005,
  STATUS_NO_MORE_FILES                = 0x80000006,
  STATUS_EA_LIST_INCONSISTENT         = 0x80000014,
  STATUS_NO_MORE_ENTRIES              = 0x8000001A,
  STATUS_STOPPED_ON_SYMLINK           = 0x8000002D,
  STATUS_UNSUCCESSFUL                 = 0xC0000001,
  STATUS_INVALID_INFO_CLASS           = 0xC0000003,
  STATUS_INFO_LENGTH_MISMATCH         = 0xC0000004,
  STATUS_INVALID_HANDLE               = 0xC0000008,
  STATUS_INVALID_PARAMETER            = 0xC000000D,
  STATUS_NO_SUCH_FILE                 = 0xC000000F,
  STATUS_INVALID_DEVICE_REQUEST       = 0xC0000010,
  STATUS_END_OF_FILE                  = 0xC0000011,
  STATUS_MORE_PROCESSING_REQUIRED     = 0xC0000016,
  STATUS_NO_MEMORY                    = 0xC0000017,
  STATUS_ACCESS_DENIED                = 0xC0000022,
  STATUS_BUFFER_TOO_SMALL             = 0xC0000023,
  STATUS_OBJECT_NAME_INVALID          = 0xC0000033,
  STATUS_OBJECT_NAME_NOT_FOUND        = 0xC0000034,
  STATUS_OBJECT_NAME_COLLISION        = 0xC0000035,
  STATUS_EAS_NOT_SUPPORTED            = 0xC000004F,
  STATUS_NONEXISTENT_EA_ENTRY         = 0xC0000051,
  STATUS_FILE_LOCK_CONFLICT           = 0xC0000054,
  STATUS_LOCK_NOT_GRANTED             = 0xC0000055,
  STATUS_NO_SUCH_LOGON_SESSION        = 0xC000005F,
  STATUS_NO_SUCH_USER                 = 0xC0000064,
  STATUS_WRONG_PASSWORD               = 0xC000006A,
  STATUS_PASSWORD_RESTRICTION         = 0xC000006C,
  STATUS_LOGON_FAILURE                = 0xC000006D,
  STATUS_INVALID_LOGON_HOURS          = 0xC000006F,
  STATUS_INVALID_WORKSTATION          = 0xC0000070,
  STATUS_PASSWORD_EXPIRED             = 0xC0000071,
  STATUS_NONE_MAPPED                  = 0xC0000073,
  STATUS_NO_TOKEN                     = 0xC000007C,
  STATUS_RANGE_NOT_LOCKED             = 0xC000007E,
  STATUS_DISK_FULL                    = 0xC000007F,
  STATUS_INSUFFICIENT_RESOURCES       = 0xC000009A,
  STATUS_IO_TIMEOUT                   = 0xC00000B5,
  STATUS_FILE_FORCED_CLOSED           = 0xC00000B6,
  STATUS_FILE_IS_A_DIRECTORY          = 0xC00000BA,
  STATUS_NOT_SUPPORTED                = 0xC00000BB,
  STATUS_INVALID_NETWORK_RESPONSE     = 0xC00000C3,
  STATUS_NETWORK_NAME_DELETED         = 0xC00000C9,
  STATUS_BAD_NETWORK_NAME             = 0xC00000CC,
  STATUS_REQUEST_NOT_ACCEPTED         = 0xC00000D0,
  STATUS_NO_SUCH_DOMAIN               = 0xC00000DF,
  STATUS_INVALID_OPLOCK_PROTOCOL      = 0xC00000E3,
  STATUS_INTERNAL_ERROR               = 0xC00000E5,
  STATUS_FILE_CORRUPT_ERROR           = 0xC0000102,
  STATUS_NOT_A_DIRECTORY              = 0xC0000103,
  STATUS_CANCELLED                    = 0xC0000120,
  STATUS_FILE_CLOSED                  = 0xC0000128,
  STATUS_PIPE_BROKEN                  = 0xC000014B,
  STATUS_LOGON_TYPE_NOT_GRANTED       = 0xC000015B,
  STATUS_INVALID_DEVICE_STATE         = 0xC0000184,
  STATUS_TRUSTED_RELATIONSHIP_FAILURE = 0xC000018D,
  STATUS_TRUST_FAILURE                = 0xC0000190,
  STATUS_NETLOGON_NOT_STARTED         = 0xC0000192,
  STATUS_FS_DRIVER_REQUIRED           = 0xC000019C,
  STATUS_USER_SESSION_DELETED         = 0xC0000203,
  STATUS_CONNECTION_DISCONNECTED      = 0xC000020C,
  STATUS_PASSWORD_MUST_CHANGE         = 0xC0000224,
  STATUS_DUPLICATE_OBJECTID           = 0xC000022A,
  STATUS_DOMAIN_CONTROLLER_NOT_FOUND  = 0xC0000233,
  STATUS_NETWORK_UNREACHABLE          = 0xC000023C,
  STATUS_VOLUME_DISMOUNTED            = 0xC000026E,
  STATUS_PKINIT_NAME_MISMATCH         = 0xC00002F9,
  STATUS_PKINIT_FAILURE               = 0xC0000320,
  STATUS_NETWORK_SESSION_EXPIRED      = 0xC000035C,
  STATUS_SMARTCARD_WRONG_PIN          = 0xC0000380,
  STATUS_SMARTCARD_CARD_BLOCKED       = 0xC0000381,
  STATUS_SMARTCARD_NO_CARD            = 0xC0000383,
  STATUS_DOWNGRADE_DETECTED           = 0xC0000388,
  STATUS_PKINIT_CLIENT_FAILURE        = 0xC000038C,
  STATUS_SMARTCARD_SILENT_CONTEXT     = 0xC000038F,
  STATUS_SERVER_UNAVAILABLE           = 0xC0000466,
  STATUS_FILE_NOT_AVAILABLE           = 0xC0000467,
  STATUS_HASH_NOT_SUPPORTED           = 0xC000A100,
  STATUS_HASH_NOT_PRESENT             = 0xC000A101
  } NTSTATUS;


/* -------------------------------------------------------------------------- **
 * Typedefs
 */

/**
 * @struct  ntstatus_Text
 * @brief   Contains the #NTSTATUS code value, status name, and error message.
 */
typedef struct
  {
  NTSTATUS    ntstatus; /**< The 32-bit NTSTATUS code.    */
  char const *msg;      /**< The error message string.    */
  char const *name;     /**< The name of the status code. */
  } ntstatus_Text;

/**
 * @struct  ntstatus_SubVals
 * @brief   A breakdown of the subfields of a 32-bit #NTSTATUS code.
 * @details
 *  The #NTSTATUS code can be broken down into five subfields:
 *  - \c severity (two bits)\n
 *    Message severity, as an integer, where: \code
 *      0 == Success  1 == Information  2 == Warning  3 == Error
 *      \endcode
 *  - \c customer (one bit)\n
 *    This should always be clear (0) in #NTSTATUS code values returned
 *    from an SMB server.  When set (1), it indicates that the error code
 *    is customer-defined (i.e., application specific).
 *  - \c N (one bit)\n
 *    Reserved; should always be clear (0).
 *  - \c facility (12 bits)\n
 *    This indicates which Windows subsystem (supposedly) generated the
 *    #NTSTATUS code.
 *  - \c errcode (16 bits)\n
 *    The actual error code part of the #NTSTATUS code.
 *
 * \b Notes
 *  - <a href="@msdocs/ms-erref/87fba13e-bf06-450e-83b1-9241dc81e781">[MS-ERREF;
 *    2.3]</a> provides more information about each of these subfields.
 *    This is archane information of academic or alchemic interest at
 *    best...but we make it available anyway.
 *  - The Facility codes are, of course, defined with respect to Windows
 *    subsystems.  Look through <a
 *    href="@msdocs/ms-erref/1714a7aa-8e53-4076-8f8d-75073b780a41">[MS-ERREF;
 *    5]</a> to find a table of Windows Facility codes and their meanings.
 */
typedef struct
  {
  uint8_t  severity;  /**< Severity of the error; 0..4, where 0 == success.   */
  uint8_t  customer;  /**< Should always be zero in errors returned from SMB. */
  uint8_t  N;         /**< Reserved, must be zero (0).                        */
  uint16_t facility;  /**< Windows facility (subsystem) identifier.           */
  uint16_t errcode;   /**< The actual error code part of the #NTSTATUS.       */
  } ntstatus_SubVals;


/* -------------------------------------------------------------------------- **
 * Function Prototypes
 */

ntstatus_Text ntstatus_getInfo( const NTSTATUS ntstatus );

char const *ntstatus_getName( const NTSTATUS ntstatus );

char const *ntstatus_getMessage( const NTSTATUS ntstatus );

char *ntstatus_getSeverityText( const NTSTATUS ntstatus );

ntstatus_SubVals ntstatus_getSubVals( const NTSTATUS ntstatus );


/* ============================= smb_ntstatus.h ============================= */
#endif /* SMB_NTSTATUS_H */
