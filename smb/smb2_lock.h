#ifndef SMB2_LOCK_H
#define SMB2_LOCK_H
/* ========================================================================== **
 *                                 smb2_lock.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Marshall/unmarshall SMB2/3 Lock exchanges.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb2_lock.h; 2020-11-27 12:35:38 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file    smb2_lock.h
 * @author  Christopher R. Hertel
 * @brief   Parse/pack SMB2/3 Lock request and response messages.
 * @date    15 Sep 2020
 * @version \$Id: smb2_lock.h; 2020-11-27 12:35:38 -0600; crh$
 * @copyright Copyright (C) 2020 by Christopher R. Hertel
 *
 *  The lock request is a bit cumbersome, but the response is just an
 *  #smb2_BaseMsg.
 *
 * @todo
 *  Consider creating a generic GUID class, of which the \c FileId would be
 *  a descendant.  How many of the different GUID types are 16 bytes long?
 *  128-bits appears to be the standard size.
 * @todo
 *  This module needs a review.  The author (that's me) was distracted
 *  while coding.  Lots going on.
 */

#include "smb2_baseMsg.h"   /* The Lock Response message is a base message. */


/* -------------------------------------------------------------------------- **
 * Defined Constants
 *//**
 * @def   SMB2_LOCK_REQ_BUFR_SIZE
 * @brief The size, in bytes, of the fixed portion of the SMB2 Lock Request
 *        \em excluding the 16-byte FileId.
 *
 * @def   SMB2_LOCK_FILEID_SIZE
 * @brief The size, in bytes, of the SMB2 \c FileId field.
 *
 * @def   SMB2_LOCK_REQ_SIZE
 * @brief The size, in bytes, of the fixed portion of the SMB2 Lock Request.
 * @details
 *  The length of the fixed portion of the SMB2/3 Lock Request message,
 *  including the \c FileId, but excluding any SMB2 Lock Element records.
 *
 * @def   SMB2_LOCK_REQ_STRUCT_SIZE
 * @brief The required structure size value.
 * @details
 *  This length value includes the length of the fixed portion of the Lock
 *  Request plus one Lock Element record.  The \c StructureSize field of the
 *  request message must contain this value.
 *
 * @def   SMB2_LOCK_ELEMENT_SIZE
 * @brief The wire size, in bytes, of the SMB2 Lock Element structure.
 */

#define SMB2_LOCK_REQ_BUFR_SIZE    8
#define SMB2_LOCK_FILEID_SIZE     16
#define SMB2_LOCK_REQ_SIZE        24
#define SMB2_LOCK_REQ_STRUCT_SIZE 48
#define SMB2_LOCK_ELEMENT_SIZE    24


/* -------------------------------------------------------------------------- **
 * Enumerated Constants
 */

/**
 * @enum SMB2_LockFlags
 *
 * \b Notes
 *  - The \c Flags field should include exactly one of [SHARED, EXCLUSIVE,
 *    UNLOCK].  These should not be used together.
 *  - The \c SMB2_LOCKFLAG_FAIL_IMMEDIATELY flag may be used when requesting
 *    a lock, but may not be combined with \c SMB2_LOCKFLAG_UNLOCK.
 *
 * @warning
 *  The \c Flags field of the first Lock Element in the array will determine
 *  how the rest of the array is processed.\n
 *  \b See: <a
 *      href="@msdocs/ms-smb2/a2f8b1cc-ebe0-4378-9da9-4e25de5c628f">[MS-SMB2;
 *      3.3.5.14]: Receiving an SMB2 LOCK Request</a>
 *
 * @var SMB2_LOCKFLAG_SHARED_LOCK
 *  Request a shared read lock on the given byte range.  If the request is
 *  successful, Other opens may also request a shared lock on the same or
 *  overlapping range.  Reads from this range are are permitted for any
 *  open, but writes on the range are not.
 *
 * @var SMB2_LOCKFLAG_EXCLUSIVE_LOCK
 *  Request an exclusive write lock on the given byte range.
 *
 * @var SMB2_LOCKFLAG_UNLOCK
 *  Relinquish an existing lock on the given byte range.  The byte range
 *  must match that of the original lock request.
 *
 * @var SMB2_LOCKFLAG_FAIL_IMMEDIATELY
 *  This flag may only be used with \c #SMB2_LOCKFLAG_SHARED_LOCK or
 *  \c #SMB2_LOCKFLAG_EXCLUSIVE_LOCK.
 */
typedef enum
  {
  SMB2_LOCKFLAG_SHARED_LOCK       = 0x00000001,
  SMB2_LOCKFLAG_EXCLUSIVE_LOCK    = 0x00000002,
  SMB2_LOCKFLAG_UNLOCK            = 0x00000004,
  SMB2_LOCKFLAG_FAIL_IMMEDIATELY  = 0x00000010
  } SMB2_LockFlags;


/* -------------------------------------------------------------------------- **
 * Typedefs
 */

/**
 * @struct  smb2_LockReq
 * @brief   SMB2/3 Lock Request  message structure.
 *
 * \b Pointers
 *  - \b \c #FileId - Pointer to the 16-byte (128-bit) File Identifier, as
 *                    returned from an SMB2 Create operation.
 *  - \b \c #Locks  - Pointer to an array of Lock Element structures.
 *                    @see \c #smb2_LockElement
 *
 * @var smb2_LockReq::StructureSize
 *      The Lock Request structure size, which is given as 48 bytes.\n
 *      At first glance, 48 seems like the wrong value here.  The message
 *      \b appears to be made up of a fixed portion (24 bytes, if you
 *      include the fixed-length \c #FileId) followed by a variable portion
 *      that contains the list of Lock Elements.  As explained briefly in
 *      [MS-SMB2; 2.2.26], the value of 48 comes from the fact that the
 *      the Lock Element structure is also a fixed size and that at least
 *      one Lock Element is required in a valid Lock Request message.  The
 *      total size of a Lock Request with one Element is 48 bytes.
 *
 *      That provides some insight into the way in which these packets are
 *      parsed internally in Windows.
 *
 * @var smb2_LockReq::LockCount
 *      The number of Lock Element structures that are appended.  The
 *      minimum value is 1.
 *
 * @var smb2_LockReq::LockSequenceNumber
 *      In the SMBv2.0.2 dialect, this field is reserved and must be zero.
 *      For all other dialects, the wire format of this field is a 4-bit
 *      unsigned integer; the value must be in the range 0..0xF.
 *
 * @var smb2_LockReq::LockSequenceIndex
 *      In the SMBv2.0.2 dialect, this field is reserved and must be zero.
 *      For all other dialects, the wire format of this field is a 28-bit
 *      unsigned integer.  However, the specifications limit this value to
 *      the range 1..64.
 *
 * @var smb2_LockReq::FileId
 *      A pointer to a 16-byte array that contains the \c FileId of the open
 *      file against which the lock(s) will be taken or released.  An
 *      SMB2_FILEID is made up of two 8-byte (64-bit) subfields, which have
 *      significance when working with Durable, Resilient, and Persistent
 *      handles.  Otherwise, the \c FileId can be viewed as a 128-bit GUID.
 *
 * @see <a href="@msdocs/ms-smb2/f1d9b40d-e335-45fc-9d0b-199a31ede4c3">[MS-SMB2;
 *         2.2.14.1]: SMB2_FILEID</a>
 * @see <a href="@msdocs/ms-smb2/eb446d90-a173-4797-9e83-d09490b203f4">[MS-SMB2;
 *         3.3.1.10]: Per Open</a> *      The FileId is an array of 16 bytes.
 *
 * @var smb2_LockReq::Locks
 *      A pointer to an array of one or more SMB2_LOCK_ELEMENT structures.
 */
typedef struct
  {
  uint16_t  StructureSize;
  uint16_t  LockCount;
  uint8_t   LockSequenceNumber;
  uint32_t  LockSequenceIndex;
  uint8_t  *FileId;
  uint8_t  *Locks;
  } smb2_LockReq;

/**
 * @struct  smb2_LockElement
 * @brief   A Lock Element represents an individual lock/unlock request.
 * @details Lock Request messages will contain an array of one or more Lock
 *          Elements.  Each Lock Element provides instructions for locking
 *          or unlocking a specific range of bytes.
 */
typedef struct
  {
  uint64_t  Offset;       /**< The offset at which the byte range begins. */
  uint64_t  Length;       /**< The length of the byte range.              */
  uint32_t  Flags;        /**< See \c #SMB2_LockFlags.                    */
  uint8_t   Reserved[4];  /**< Reserved-MB0.                              */
  } smb2_LockElement;

/**
 * @typedef smb2_LockResp
 * @brief   SMB2/3 Lock Response message structure.
 * @details This maps directly to a \c #smb2_BaseMsg.
 * @see     #smb2_BaseMsg
 */
typedef smb2_BaseMsg smb2_LockResp;


/* -------------------------------------------------------------------------- **
 * Function Prototypes
 */

int smb2_parseLockReq( uint16_t      const dialect,
                       uint8_t      *const msg,
                       size_t        const msgLen,
                       smb2_LockReq *const lockReq );

int smb2_packLockReq( uint16_t      const dialect,
                      smb2_LockReq *const lockReq,
                      uint8_t      *const bufr,
                      uint32_t      const bSize );

int smb2_parseLockElement( uint16_t          const dialect,
                           uint8_t          *const msg,
                           size_t            const msgLen,
                           smb2_LockElement *const lockElm );

int smb2_packLockElement( uint16_t          const dialect,
                          smb2_LockElement *const lockElm,
                          uint8_t          *const bufr,
                          uint32_t          const bSize );

int smb2_parseLockResp( uint16_t       const dialect,
                        uint8_t       *const msg,
                        size_t         const msgLen,
                        smb2_LockResp *const lockResp );

int smb2_packLockResp( uint16_t       const dialect,
                       smb2_LockResp *const lockResp,
                       uint8_t       *const bufr,
                       uint32_t       const bSize );


/* =============================== smb2_lock.h ============================== */
#endif /* SMB2_LOCK_H */
