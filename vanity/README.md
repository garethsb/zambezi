What's Here?
------------

Odds and ends that don't belong elsewhere.  Currently:
- Project logo image files.
- A slightly customized HTML header for Doxygen.
- This README file.
____
$Id: README.md; 2020-06-01 16:10:47 -0500; crh$
