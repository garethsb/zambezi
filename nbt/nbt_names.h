#ifndef NBT_NAMES_H
#define NBT_NAMES_H
/* ========================================================================== **
 *                                 nbt_names.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: NetBIOS over TCP (NBT) Transport; NBT Names.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: nbt_names.h; 2020-11-01 20:34:46 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 *  - The NBT Name, Datagram, and Session services all make use of NBT names.
 *
 * ========================================================================== **
 *//**
 * @file    nbt_names.h
 * @author  Christopher R. Hertel
 * @brief   NBT Name encoding and decoding.
 * @date    23-Apr-2020
 * @version \$Id: nbt_names.h; 2020-11-01 20:34:46 -0600; crh$
 * @copyright Copyright (C) 2020 by Christopher R. Hertel
 *
 * @details
 *  This module provides the tools needed to compose, encode, decode, and
 *  parse NBT Names.
 *
 *  There is a lot of room for confusion here, because of the fluid
 *  movement between ASCIIZ strings (C strings) and length-delimited
 *  substrings.  In many cases, a NUL octet could be a string terminator,
 *  or it could be a valid octet in a name, or it could be an empty label
 *  (where a "label" is a length-delimited substring within a longer
 *  string).
 *
 *  <a href="http://ubiqx.org/cifs/NetBIOS.html">[IMPCIFS; 1]</a> provides
 *  a detailed explaination of what this all means and how it all works.
 *
 *  Some basic guidelines:
 *  - NBT was created back in the days before Unicode.  8-bit characters
 *    are assumed, but modern Windows systems use a 16-bit Unicode format
 *    that introduces NUL octets into text strings.  Bad things can happen,
 *    and they need to be handled.
 *  - In almost all cases, strings are likely to be NUL terminated ASCIIZ
 *    strings.  On the off chance that a NUL needs to be included in a
 *    NetBIOS name, however, these functions provide sensible work-arounds.
 *
 *  Some terminology:
 *  - <B>NetBIOS Name</B>\n
 *    NetBIOS Names are 16 octets in length.  By convention, the 16th octet
 *    is reserved for a "suffix" octet, that is used to identify the type of
 *    service associated with the name.
 *  - <B>NetBIOS Base Name</B>\n
 *    This is a "user-friendly" name for a service or machine or other
 *    object in a NetBIOS-based network.  It is a maximum of 15 8-bit
 *    characters in length.
 *  - <B>Scope Id</B>\n
 *    Similar in concept to a VLAN identifier.  The Scope Id is used to
 *    limit the scope in which a NetBIOS Name can logically exist.  The
 *    Scope Id follows the format of DNS domain names, as defined in early
 *    RFCs.  That is, it is a series of short names (labels) delimited by
 *    dots.  Eg.: <tt>feldspar.omnibus.noodle</tt>
 *  - <B>NBT Name</B>\n
 *    The NBT Name is an encoded form of the NetBIOS Name and Scope Id.
 *    The first level of encoding transforms the NetBIOS Name into a series
 *    of octets in the range 'A'..'P', thus ensuring that the wire format
 *    of the NetBIOS name is composed of printable ASCII characters.  The
 *    scope is then appended to the encoded name using a dot as a delimiter.
 *
 *    The second level of encoding converts each part of the fully-qualified
 *    name into a series of length-delimited substrings, each with a maximum
 *    length of 63 octets.
 *
 *  This is all described with more clarity and detail in <a
 *  href="http://ubiqx.org/cifs/NetBIOS.html">[IMPCIFS; 1]</a> which, in
 *  turn, refers to the relevant sections of <a
 *  href="https://www.rfc-editor.org/info/std19">[STD19]</a>.
 *
 * @see
 *  <a href="http://ubiqx.org/cifs/NetBIOS.html">[IMPCIFS; 1] NBT: NetBIOS
 *  over TCP/IP</a>
 * @see
 *  <a href="http://www.ietf.org/rfc/rfc1001.txt">[RFC1001]: Protocol
 *  Standard for a NetBIOS Service on a TCP/UDP Transport: Concepts and
 *  Methods</a>
 * @see
 *  <a href="http://www.ietf.org/rfc/rfc1002.txt">[RFC1002]: Protocol
 *  Standard for a NetBIOS Service on a TCP/UDP Transport: Detailed
 *  Specifications</a>
 * @see
 *  <a href="https://www.rfc-editor.org/info/std19">[STD19]:
 *  [RFC1001] and [RFC1002] together comprise IETF STD #19</a>
 * @see
 *  <a href="@msdocs/ms-nbte/3461cfa8-3d28-4fa3-8163-131bf1046fa3">[MS-NBTE]:
 *  NetBIOS over TCP (NBT) Extensions</a>
 */

#include <ctype.h>      /* For toupper(3), isalpha(3), and related. */
#include <stdbool.h>    /* Standard boolean type.                   */

#include "nbt_common.h" /* Subsystem common header.                 */


/* -------------------------------------------------------------------------- **
 * Defined Constants
 *//**
 * @def   nbt_NB_NAME_MAX
 * @brief User-friendly NetBIOS Name length.
 * @details
 *  The maximum string length of a base NetBIOS name.\n
 *  Originally, NetBIOS names were a fixed 16 octets in length.  By
 *  convention, however, the last octet was reserved for a "suffix octet".
 *  Thus, what we call "base" NetBIOS names are a maximum of 15 octets,
 *  which leaves space for the suffix.  This particular constant is 16,
 *  which leaves room for a NUL string terminator.  The extra octet <b>is
 *  not</b> used for the NBT suffix; that's added separately.  End-users
 *  are typically unaware of the existance or purpose of padding or the
 *  suffix octet.
 * @see #nbt_NbName
 *
 * @def   nbt_L1_NB_NAME_MAX
 * @brief L1 Encoded NetBIOS Name max length.
 * @details
 *  This is the maximum string length of an L1 Encoded NetBIOS Name; 33
 *  octets to account for a terminating NUL.  The scope is not considered
 *  in this measure.
 * @see #nbt_L1Name
 *
 * @def   nbt_L2_NB_NAME_MIN
 * @brief The minimum length of an L2 Encoded NBT Name.
 * @details
 *  The minimum length of an L2 Encoded NBT Name.\n
 *  This is the length of an NBT name if the scope is the empty string ("").
 *  That's 1 octet for the name length, 32 octets for the encoded NetBIOS
 *  name, and 1 octet for a NUL trailing label length.
 *
 * @def   nbt_L2_NAME_MAX
 * @brief NBT name (L2 Name + Scope) max length.
 * @details
 *  The maximum length of an <b>L2 Encoded</b> NBT name, including the
 *  Scope.\n
 *  Note that the maximum string length of an \b L1 encoded NBT name is 254
 *  octets, but that does not account for the NUL octet (the root label), so
 *  255 octets are actually needed for L2.  The encoded NBT name is 255
 *  (0xFF) octets max, including the terminating NUL label.
 * @see #nbt_L2Name
 * @see <a href=
 *    "https://www.ietf.org/rfc/rfc1002.html#section-4.1">[RFC1002; 4.1]</a>
 */

#define nbt_NB_NAME_MAX    16
#define nbt_L1_NB_NAME_MAX 33
#define nbt_L2_NB_NAME_MIN 34
#define nbt_L2_NAME_MAX    255


/* -------------------------------------------------------------------------- **
 * Macros
 */

/**
 * @def     nbt_L2NameLen( name )
 * @brief   Return the name length of an L2 encoded NBT name.
 * @param   name  - The NBT name to be length-counted.
 * @returns The length of \p name, including the final NUL (the root label).
 *          The result is returned as an \c int.
 * @details
 *  Encoded names must contain exactly one NUL octet, which must be the
 *  final octet of the L2 encoded name.
 * @hideinitializer
 */
#define nbt_L2NameLen( name ) ((int)(1+strlen(name)))

/**
 * @def     nbt_UpString( S )
 * @brief   Convert an ASCII string to upper case using #nbt_UpCaseStr().
 * @param   S - A pointer to a character array to be up-cased.
 * @returns The length of \p S or, if negative, an error code.
 * \b Errors
 *    - \b \c nbt_errNullInput - Indicates that \p S was NULL.
 *
 * @details
 *  This macro converts the string in-place, up to the terminating NUL octet
 *  (which needs no conversion).
 * @hideinitializer
 */
#define nbt_UpString( S ) nbt_UpCaseStr( (S), NULL, -1 )


/* -------------------------------------------------------------------------- **
 * Typedefs
 */

/**
 * @brief   A fixed-length buffer sufficent to store a base NetBIOS name.
 * @details
 *  This is a fixed-size octet array for holding un-encoded NetBIOS
 *  names.  The array is 16 octets long.  That's 15 octets for the name and
 *  one octet for a NUL string terminator, which \b must be present.
 *
 * \b Note:
 *  - As explained elsewhere, NetBIOS names are supposed to be 16 octets in
 *    length, but the 16th is reserved as the "suffix".  Thus, the maximum
 *    length of a NetBIOS name is effectively 15 octets.
 */
typedef uint8_t nbt_NbName[nbt_NB_NAME_MAX];

/**
 * @brief   A fixed-length buffer sufficient to store an L1-encoded NetBIOS
 *          name.
 * @details
 *  This is a fixed-size array of octets sufficient to store an L1-encoded
 *  NetBIOS name (`#nbt_L1_NB_NAME_MAX` octets long).  NetBIOS names are, at
 *  most 15 octets in length.  Names shorter than 15 octets are padded, and
 *  the suffix is added, resulting in a fixed length of 16 octets.  The L1
 *  encoding process doubles this.  The result, then, has a string length of
 *  32 octets.  One more octet is available to store a string-terminating
 *  NUL.
 */
typedef uint8_t nbt_L1Name[nbt_L1_NB_NAME_MAX];

/**
 * @brief   A fixed-length buffer sufficent for a fully-encoded NBT name.
 * @details
 *  This is a fixed-size octet array for holding L2 encoded NBT names
 *  (`#nbt_L2_NAME_MAX` octets long).  L2 encoded NBT names are always NUL
 *  terminated, except that the NUL octet is, <em>part of the name</em>.
 *  The terminating NUL is the root DNS label.
 */
typedef uint8_t nbt_L2Name[nbt_L2_NAME_MAX];

/**
 * @brief   A structure to store the broken-out components of an NBT Name.
 * @details
 *  This structure keeps track of the various octets and pieces that are
 *  needed for building an NBT name from a NetBIOS name, padding, suffix
 *  octet, and Scope ID.  Note that the #nbt_NameRec::name field is a
 *  length-delimited (not NUL-terminated) string.
 */
typedef struct
  {
  uint8_t  nameLen;   /**< Length of the octet string in \c name. */
  uint8_t *name;      /**< NetBIOS name as an octet string.       */
  uint8_t  pad;       /**< Padding octet value.                   */
  uint8_t  sfx;       /**< Suffix octet value.                    */
  uint8_t *scopeId;   /**< ScopeID as a NUL-terminated string.    */
  } nbt_NameRec;



/* -------------------------------------------------------------------------- **
 * Function Prototypes
 */

int nbt_UpCaseStr( uint8_t *src, uint8_t *dst, int max );

int nbt_CheckNbName( const uint8_t *name, const int len );

int nbt_CheckScope( const uint8_t *scope );

int nbt_CheckL2Name( const uint8_t *src, int srcpos, const int srcmax );

int nbt_CheckLSP( uint8_t lablen );

int nbt_L1Encode( uint8_t *dst, const nbt_NameRec *src );

int nbt_L1Decode( uint8_t       *dst,
                  const uint8_t *src,
                  const int      srcpos,
                  const uint8_t  pad,
                  uint8_t       *sfx );

int nbt_L2Encode( uint8_t *dst, const nbt_NameRec *namerec );

int nbt_L2Decode( uint8_t *dst, const uint8_t *src, int srcpos );

int nbt_EncodeName( uint8_t           *dst,
                    const int          dstpos,
                    const int          dstlen,
                    const nbt_NameRec *namerec );


/* =============================== nbt_names.h ============================== */
#endif /* NBT_NAMES_H */
